Odpowiedzi do zadań z (wybranych) zajęć
#######################################


Zaj 1
=====


Zad B
-----

::

  apt-cache search -n ssh | grep server

Są dwie paczki które instalują (ten sam server ssh):

* ``task-ssh-server``
* ``openssh-server ``

Instalacja::

  apt-get update
  apt-get install openssh-server

Zad C
-----

Znaleźć linie (ew. dodać) o treści::

  PermitRootLogin no
  DenyUsers foo

do pliku ``/etc/sshd_config``

Zad D
-----

Możecie przypomnieć studentom o ``scp``.


Zad E
-----

::

  ufw default deny
  ufw enable # SSH nie działa
  ufw allow 22 # działa
  ufw reset # wyłączamy
  ufw disable # wyłączamy

Zad F
-----

.. note::

  Najpierw dodajemy kolejny interfejs sieciowy do maszyny wirtualnej, musi być to
  ``Bridged Adapter``, przypięty do ``eth0`` na maszynie matce. Będzie on widoczny
  jako ``eth2`` na dziecku,  by pobrać adres za pomocą DHCP na dziecku należy
  napisać::

    dhclient eth2

  Oczywiście normalnie powinno się np. edytować ``/etc/network/interfaces``, albo
  zmienić w ``network-manager``, ale tak jest prościej (a network manager słabo
  radzi sobie z trzema takimi samymi kartami sieciowymi)

By udostępnić:

* Dodać do etc/exports::

  /home/student/foo 192.168.1.0/24(rw,sync)

* By używać::

    mkdir /tmp/foo
    mount 192.168.1.XX:/home/student/foo /tmp/foo


Zaj 5
=====



Zad B
-----

::

  apt-cache search -n ssh | grep server

Są dwie paczki które instalują (ten sam server ssh):

* ``task-ssh-server``
* ``openssh-server ``

Instalacja::

  apt-get update
  apt-get install openssh-server

Zad C
-----

Znaleźć linie (ew. dodać) o treści::

  PermitRootLogin no
  DenyUsers foo

do pliku ``/etc/sshd_config``

Zad D
-----

Możecie przypomnieć studentom o ``scp``.


Zad E
-----

::

  ufw default deny
  ufw enable # SSH nie działa
  ufw allow 22 # działa
  ufw reset # wyłączamy
  ufw disable # wyłączamy

Zad F
-----

.. note::

  Najpierw dodajemy kolejny interfejs sieciowy do maszyny wirtualnej, musi być to
  ``Bridged Adapter``, przypięty do ``eth0`` na maszynie matce. Będzie on widoczny
  jako ``eth2`` na dziecku,  by pobrać adres za pomocą DHCP na dziecku należy
  napisać::

    dhclient eth2

  Oczywiście normalnie powinno się np. edytować ``/etc/network/interfaces``, albo
  zmienić w ``network-manager``, ale tak jest prościej (a network manager słabo
  radzi sobie z trzema takimi samymi kartami sieciowymi)

By udostępnić:

* Dodać do etc/exports::

  /home/student/foo 192.168.1.0/24(rw,sync)

* By używać::

    mkdir /tmp/foo
    mount 192.168.1.XX:/home/student/foo /tmp/foo


Zaj 6
=====


Zad A
*****

::

  adduser test
  adduser test sudo

Zad B
*****

::

  adduser user
  visudo

Do sudoers dodać::

  user    ALL= /sbin/reboot


Zad E
*****

U mnie plik wygląda tak::

  server {
    listen 80;
    listen [::]:80;

    root /usr/share/doc;

    # Add index.php to the list if you are using PHP
    index index.html index.htm index.nginx-debian.html;

    server_name onet.pl;

    location / {
      # First attempt to serve request as file, then
      # as directory, then fall back to displaying a 404.
      autoindex on;

    }

  }



Zad F
*****

::

  telnet localhost 80
  GET / HTTP/1.1
  Host: localhost

  telnet localhost 80
  GET / HTTP/1.1
  Host: onet.pl

Zadanie G
*********

Muszą przeklepać kilka kilka komend. Hasło w oddzielnym e-mailu.
