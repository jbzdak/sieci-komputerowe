Kolokwia
========

Edycja 2013
-----------

Zeszłoroczne kolokwia u Jacka Bzdaka (**uwaga** jesteśmy w trakcie zmian w programie
więc zakres tematyczny na kolokwiach może się zmienić!).

* :download:`Kolokwium o <kolokwia/2013/sk-kol-1-o.odt>`
* :download:`Kolokwium x <kolokwia/2013/sk-kol-1-x.odt>`
* :download:`Kolokwium y <kolokwia/2013/sk-kol-1-y.odt>`
* :download:`Kolokwium z <kolokwia/2013/sk-kol-1-z.odt>`

Edycja 2014
-----------

* :download:`Kolokwium i <kolokwia/sk-kol-1-i.odt>`
* :download:`Kolokwium z <kolokwia/sk-kol-1-j.odt>`
* :download:`Kolokwium p <kolokwia/sk-kol-1-p.odt>`
* :download:`Kolokwium z <kolokwia/sk-kol-1-z.odt>`
* :download:`Kolokwium u <kolokwia/sk-kol-1-popr.odt>`
