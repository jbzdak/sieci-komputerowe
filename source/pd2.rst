Praca domowa 2: System sieciowy
===============================

Uwagi ogólne
------------

* Wasz program powinien realizować **użyteczną usługę** oraz powinien posiadać
  **wartość użytkową**.
* Jeśli program zakłada stworzenie kilku programów (usług sieciowych) powinny
  być one zaimplementowane w przynajmniej dwóch różnych językach programowania.
* Wszystkie programy muszą działać pod Linuksem Debian.
* Projekty powinny są wykonywane w zespołach dwu osobowych, poza projektami
  trzy osobowymi (oznaczone w opisie)
* Możliwe jest oddanie projektu nie spełniającego wszystkich wymagań, ale
  powinien on **mieć wartość użytkową**.
* W jednej grupie tylko jeden zespół może wybrać dany projekt (poza projektem
  na niższą ocenę, który mogą wybrać wszyscy).
* Razem z programem należy wykonać opis dokładny protokołu, opis ten będzie
  analizowany podczas oceny.
* Ocenie podlega:

  * Funkcjonalność systemu
  * Wybór odpowiednich protokołów sieciowych
  * Elegancja protokołu komunikacji
  * Posiadanie dokładnego opisu protokołu

Tematy
------

W ramach każdego zadania dokonają państwo re implementacji
*istniejącego* protokołu komunikacji. Przy czym Państwa wersja
będzie miała takie cechy:

* Będzie uproszczona względem oryginału
* Nie będzie zawierała żadnych funkcjonalności związanych z bezpieczeństwem.


Zero-Conf chat protocol --- projekt na niższą ocenę
***************************************************


**Projekt na 3.0**

Program pozwala na chatowanie z użytkownikami po sieci lokalnej za pomocą wiadomości
Broadcast. Dokładniej: za pomocą broadcastów wykrywamy jacy użytkownicy są widoczni
w sieci lokalnej --- następnie możemy rozmawiać z nimi już bezpośrednio.

Program musi spełniać takie wymagania:

* Po jego uruchomieniu (i podaniu mojego loginu) wyświetla listę
  wszystkich loginow które aktualnie są podłączone do sieci lokalnej (na przykład
  sieci Wydzialu Fizyki).
* Musi być możliwość taka by kilka osób mogło chatować z tego samego komputera
  (bez żadnej synchronizacji między nimi). Przykladowo: Adam i Basia łączą się
  przez SSH na komputer ``nuclab01`` i mogą oddzielnie brać udział w naszej
  sieci rozmawiając np. z Cecylią i Darkiem.
* Fajnie by było gdyby program posiadał graficzny interfejs użytkownika ;)

**Wersja na 4.0**

Przedstawione są dwa różne klienty napisane w różnych językach, klienty te
są kompatybilne --- tj. mogą rozmawiać ze sobą.


Gawariet-Gawariet
*****************

Czyli program pozwalający na chatowanie między użytkownikami.

System składa się z dwóch elementów: serwera i klienta.

Każdy użytkownik ma swój login oraz hasło. Klient by wysłać wiadomość do innego
użytkownika musi:

* Połączyć się do serwera wysyłając login i hasło.
* Serwer (oczywiście!) sprawdza to hasło
* Wysłać pakiet danych zawierający login użytkownika do którego chce wysłać 
  wiadomość oraz treść wiadomości.
* Serwer wysyła teraz wiadomość to drugiego użytkownika (który może odpowiedzieć)
  jeśli drugi użytkownik nie jest zalogowany pierwszy dostaje informację o tym

Dodatkowo:

* Istnieje funkcjonalność znajomych --- by Adam mógł rozmawiać z Beatą, najpierw
  muszą stać się znajomymi.
* Klient może pobrać listę swoich znajomych oraz informacje o tym czy są aktualnie
  zalogowani z serwera.


Rudolph -- server discovery system
**********************************

W dużych serwerowniach problemem okazuje się być inwentaryzacja tego co jest
podłączone do sieci w danej serwerowni. Chcielibyśmy zrobić system który
automatycznie podaje informacje o serwerach.

Będzie działał on tak: nowy serwer po uruchomieniu wysyła zapytanie
(do całej sieci!): "Czy jest tutaj może system nadzorcy". Na zapytanie to
odpowiada tylko system nadzorcy który:

* Zapisuje sobie IP serwera który się dołączył
* Wysyła do tego serwera zapytanie o jego podstawowe parametry

Na takie pytanie serwer odpowiada wysyłając parametry.

Zadania dla systemu nadzorcy:

* Odpowiadanie na pytania: "Czy jest tutaj może system nadzorcy"
* Przechowywanie listy serwerów które są nadzorowane
* Odpytywanie co np. 5 minut wszystkich serwerów z listy
* Przechowywanie informacji o serwerach.
* Przechowywanie informacji kiedy dany serwer był online (kiedy ostatni raz
  wysłał komunikat do nadzorcy).
* Serwery są identyfikowane za pomocą adresu MAC karty sieciowej.
* Dane o serwerach powinny być przechowywane w pamięci trwałej.

Zadania dla klienta:

* Wysyłanie pytania: "Czy jest tutaj może system nadzorcy"
* Wysyłanie listy informacji o serwerze:

 * Adres MAC karty sieciowej
 * Ilość rdzeni
 * Ilość RAM
 * Zajętość procesora
 * Lista 20 procesów zajmujących najwięcej czasu procesora w danej chwili?
 * Lista 20 procesów zajmujących najwięcej pamięci RAM

.. note::

    Oczywiście istnieje już gotowy system który spełnia te wymagania,
    jest to np. `ralph <http://ralph.allegrogroup.com/>`_.


PTTP(U) --- Plain Text Transfer Protocol Unsafe
***********************************************

Protokół HTTPS pozwala na uzyskanie bezpiecznego połączenia z serwerem HTTP,
(czyli zapewnia on poufność danych oraz ich integralność).

Protokół PTTP (Plain Text Transfer Protocol) jest uproszczoną wersją HTTP,
składa się on z jednej komenty::

    GET /path/to/file PTPP/1.0

serwer po jej otrzymaniu odeśle plik, kończąc jego nadawanie za pomocą ciągu
znaków::

    <<PTTP END>>

Państwa program dodatkowo będzie pozwalał na "szyfrowanie" protokołu PTTP za pomocą 
algorytmu
`Base64 <http://pl.wikipedia.org/w/index.php?title=Base64&oldid=38391388>`_
(algorytm ten zamienia dowolny ciąg bajtów na
ciąg zawierający tylko znaki które da się wyświetlić w ramach zestawu
znaków ASCII).

Mają państwo za zadanie wykonać serwer PPTP(U) oraz klienta PPTU.

Serwer PPTP(U) ma takie funkcjonalności:

* Przyjmuje połączenia TCP
* Można zdefiniować katalog jaki serwuje, jeśli katalogiem tym będzie:
  ``/var/pttp``, a klient wyśle ``GET /path/to/file PTPP/1.0``, zostanie
  mu wysłana zawartość pliku: ``/var/pttp/path/to/file``.
* Jeśli serwerowi zostanie wysłane ścieżka do katalogu serwer wyśle listę 
  możliwych do pobrania plików i podkatalogów w danym katalogu.
* Serwer ten nasłuchuje na dwóch portach P1 oraz P2.
* Żądania wysyłane na port P1 są wysyłane tekstem jawnym.
* Żądania jak i odpowiedzi wysyłane na port P2 są enkodowane za pomocą base64.

Klient powinien mieć takie funkcjonalności:

* Pozwala pobrać zawartość plików za pomocą dwóch protokołów: PPTP oraz PPTPU.
* Po wybraniu zasobu ``pptp://host/path/to/file`` wyśle żądanie
  ``GET /path/to/file PTPP/1.0`` na serwer ``host`` i komunikacja
  będzie się odbywała tekstem jawnym.
* Po wybraniu zasobu ``pptpu://host/path/to/file`` wyśle żądanie
  ``R0VUIC93aWtpL1BsYWNfTmllYmlhJUM1JTg0c2tpZWdvX1Nwb2tvanUgSFRUUC8xLjEK``
  na serwer ``host`` i komunikacja
  będzie się odbywała za pomocą enkodowania base64.
* Dodatkowym atutem będzie to że program posiada graficzny interfejs użytkownika.

.. warning::::

    Oczywiście Base64 nie udostępnia żadnego bezpieczeństwa!


.. note::

    Do wykonania kodowania Base64 w linuksie służy polecenie ``base64``:

    .. code-block:: bash

        echo GET /wiki/Plac_Niebia%C5%84skiego_Spokoju HTTP/1.1 | base64
        R0VUIC93aWtpL1BsYWNfTmllYmlhJUM1JTg0c2tpZWdvX1Nwb2tvanUgSFRUUC8xLjEK

        echo R0VUIC93aWtpL1BsYWNfTmllYmlhJUM1JTg0c2tpZWdvX1Nwb2tvanUgSFRUUC8xLjEK | base64 -d
        GET /wiki/Plac_Niebia%C5%84skiego_Spokoju HTTP/1.1


Rumba ---  czyli bezkonfiguracyjna wymiana plików
*************************************************

W tym zadaniu nie ma serwera --- ale klienci mogą wymieniać się plikami.

Każdy klient określa listę katalogów które "udostępnia" innym systemom.

Po włączeniu klient wysyła pytanie do sieci lokalnej "Kto obsługuje protokół Rumba",
wszystkie inne klienty odpowiadają że obsługują.

Potem klient może wysłać do inneogo klienta jedno z dwóch żądań:

* Podaj mi listę plików i katalogów które udostępniasz
* Pobrania danego pliku z danego katalogu

Użytkownik może:

* Pobrać listę komputerów obsługujących protokół Rumba
* Wyświetlić listę plików i katalogów na danym komputerze
* Pobrać plik


E-Goat
******

Bardzo przydatną usługą są sieci wymiany plików peer-to-peer 
działają one w następujący sposób:

* Serwer zawiera informację o tym jakie pliki mają klienci (ale nie zawiera plików)
* Klienci mogą wymieniać się plikami bezpośrednio
* Istnieje metoda weryfikacji poprawności przesłanego pliku.

W Waszym systemie pliki będą identyfikowane za pomocą sum kontrolnych ``sha512``,
suma kontrolna funkcją która ma następujące cechy:

* Przypisuje długiemu ciągowi bajtów (np. zawartości pliku) krótki ciąg bajtów
  (czyli sumę kontrolną)
* Trudno jest stworzyć ciąg bajtów który będzie miał zadaną sumę kontrolną.

.. note::

    By uzyskać sumę kontolną pliku należy w linuksie wisać:

    .. code-block:: bash

        jb ~ $ sha512sum rand.data
        c320503fbc8b1970d5595e03ca43c8a3f0252bf6c78d1783e61ca1b0e18a7a03461d6b25afa219fe2cd4fdd879e2b50144469a35b388c903c2f8f4a4428a30d6  rand.data

Rozwiązanie składa się z dwóch programów: klienta i serwera.

Klient ma następujące funkcjonalności:

* Podajemy mu katalog, wszystkie pliki w tym katalogu są ``Udstępnione``
* Po wyliczeniu sum ``SHA512`` wszystkich plików wysyła listę plików do serwera
* By pobrać plik klient musi wysłać do serwera sumę SHA512 tego pliku
  następnie uzyska listę klientów którzy zgłosili że posiadają taki plik.
* Klient łączy się do jednego z tych klientów i pobiera za pomocą żądany plik.
  Po pobraniu sprawdza czy otrzymał plik o prawidłowej sumie kontrolnej.

The Cake Router (Cakes have layers too)
***************************************

Jedną z najlepszych metod omijania cenzury Internecie (oraz zapewniania
sobie anonimowości jest sieć TOR (The Onion Router). Teraz przytoczę
jej krótki opis. Są w niej trzy rodzaje bytów:

* Klienci
* Węzły pośredniczące
* Węzły końcowe

Połączenie klienta działa tak:

Klient łączy się do jednego z węzłów pośredniczących (powiedzmy P1) i
wysyła pakiet danych, Pakiet ów jest zaszyfrowany tak że tylko P1 może go
odczytać po odszyfrowaniu P1 otrzymuje dwie informacje: adres kolejnego
węzła P2, oraz zaszyfrowaną paczkę danych którą P1 powinien wysłać do P2, P2
otrzymuje pakiet który odszyfrowuje (by uzyskać adres P3) i tak dalej.
Węzeł PN po odszyfrowaniu ma wysłać pakiet do węzła końcowego (WK), który
wysła go w postaci odszyfrowanej do prawdziwego odbiorcy.

Odbiorca myśli że kontaktuje się z węzłem końcowym. Gdy wyśle dane do niego
te są powtarzają tą samą drogę (ale w odwrotnym kierunku), tj: dane przechodzą
przez takie komputery WK -> PN -> P(N-1) -> (...) -> P1 -> Klient.

Zasadnicze pytanie brzmi skąd WK wie jaką drogą posłać dane z powrotem ---
odpowiedź jest prosta: nie wie! Odsyła dane tam skąd je dostał.

Państwa system powinien działać podobnie, tylko że nie musi zawierać szyfrowania.

Spełnia on takie wymagania:

* Umożliwiamy przesyłanie komunikatów UDP
* Klient posiada listę wszystkich węzłów pośredniczących
* Wybiera w niej trasę przez N węzłów pośredniczących
* Do P1 wysyła komunikat zawierający listę węzłów pośrednich, ostatecznego
  odbiorce oraz dane do przesłania.
* Każdy z węzłów PN pamięta od kogo dostał w danym połączeniu informację.
* Możliwe by klient wysłał pakiet UDP od siebie do innego hosta (i otrzymał
  odpowiedź). Pakiet (i odpowiedź) musi przejść przez dwa węzły pośredniczące
  oraz węzeł końcowy.
* Sieć jest w stanie obsłużyć dwóch równolegle pracujących klientów.

**Dane formalne**

Projekt na trzy osoby.

