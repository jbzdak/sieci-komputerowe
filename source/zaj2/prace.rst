Tematy prac domowych
====================

.. contents:: Spis Treści


Backup Solution 1
-----------------

Skrypt basha, który dokonuje kopii zapasowej plików użytkownika i dodatkowo
spełnia następujące wymagania.

1. Program jest konfigurowalny za pomocą plików konfiguracyjnych (wedle
   Państwa uznania) znajdujących się w katalogu ``~/.config/sk/back1``.

   Konfigurowalny jest:

   * Zestaw katalogów, których kopie wykonujemy.
   * Katalog docelowy, do ktorego wykonujemy kopię zapasową.
   * Pliki których (rozszerzenia, albo wyrażenia regularne) których kopii nie
     robimy
   * Maksymalny rozmiar plików które są kopiowane.

2. Dodatkowo program będzie:

   * Kompresował katalogi i pliki
   * (opcjonalnie) szyfrował je

Backup Solution 2
-----------------
Skrypt basha, który dokonuje kopii zapasowej plików użytkownika i dodatkowo
spełnia następujące wymagania.


1. Program jest konfigurowalny za pomocą plików konfiguracyjnych (wedle
   Państwa uznania) znajdujących się w katalogu ``~/.config/sk/back2``.

   Konfigurowalny jest tylko zestaw katalogów, których kopie wykonujemy:

   * Zestaw katalogów, których kopie wykonujemy.
   * Katalog docelowy, do ktorego wykonujemy kopię zapasową.

2. Program działa następująco:

   1. Pobiera z katalogu z kopią zapasową plik zawierający datę ostatniej
      synchronizacji (ścieżką do tego pliku może być np.  ``katalog_docelowy/.last-sync``)
   2. Tworzy w katalogu docelowym katalog o nazwie zawierającej aktualną datę.
   3. Przesyła tam wszystkie pliki których czas modyfikacji lub stworzenia
      jest **po** dacie ostatniej synchronizacji.
   4. Program zapisuje datę ostatniej synchronizacji.


Start/stop deamon
-----------------

Program, który służy do zarządzania stanem serwera.

Powiedzmy, że mamy program startujący serwer ``server.sh``, nasz program będzie
miał następujące polecenia:

* ``start`` który startuje ``server.sh``, jeśli ``server.sh`` już jest
  uruchomiony to polecenie informuje o błędzie.
* ``stop`` który wyłącza ``server.sh`` jesli ``server.sh`` nie jest uruchomiony
  program informuje o błędzie.
* ``restart``, stopuje i startuje program
* ``status`` wyświetla status programu.

Program wykrywa to czy ``server.sh`` jest uruchomione za pomocą tzw. pidfile.

Pid Files
^^^^^^^^^

PID to inaczej **Proces ID**, czyli numeryczny identyfikator procesu w linuksie.

By uruchomić program ``sleep 1000``` w tle należy (polecenie sleep powoduje
zarzymanie wykonania programu na zadaną liczbę sekund):

.. code-block:: bash

  sleep 1000 & # Znak: & powoduje wysłanie polecenia "w tło"
  pid=$! # Pobiera ID urucomionego procesu
  disown # Powoduje że zadania "w tle" nie zostaną wyłączone po wylogowaniu się z sesji

Pid file to pliik zawierający id uruchomionego procesu, nasz skrypt będzie
po wykonaniu ``start`` zapisze ``ID`` procesu do pliku, i przy następnym wykoaniu
będzie sprawdzać czy program o zapisanim ``pid`` ciągle jest wykonywany.


Program sprawdzający czy inny program jest uruchomiony
------------------------------------------------------

Skryupt basha, który służy do zarządzania stanem serwera.

Powiedzmy, że mamy program startujący serwer ``server.sh``, nasz program będzie
miał jedno proste zadanie: co sekundę będzie sprawdzać czy serwer jest
uruchomiony, a jeśli nie jest będzie go uruchamiać ponownie.

Program powinien korzystać z pidfiles (patrz: ``Start/stop deamon``).

Program pobierający obrazki z serwera WWW
-----------------------------------------

Program pobierający wszystkie obrazki z zadanej strony WWW.

Program powinien:

* Pobrać stronę podaną "z linii komend" do pliku tymczasowego
* Znaleźć w niej adresy obazków
* Pobrać pliki obrazków
* Usunąć plik z treścią strony

Program sprawdzający aktualność strony WWW
------------------------------------------

Program posiada w konfiguracji listę stron WWW.

* Podczas wywołania pobiera każdą z nich i sprawdza czy jej treść się nie
  zmieniła.
* Program zapamiętuje zmienione strony
* Program wyświetla listę stron które zmienły się od ostatniego razu

Program implementujący folder "Kosz" w bashu
--------------------------------------------

Proszę napisać skrypt pozwalający na usuwanie plików. Pliki przekazywane w argumencie należy kom-
presować (jeśli nie są jeszcze skompresowane) i przenosić do katalogu-śmietnika. Przy każdym wywołaniu
należy usuwać ze śmietnika pliki starsze niż 24 godziny. Należy umożliwić usuwanie plików oraz całych
katalogów.

Informacja o zmianie parametrów komputera
-----------------------------------------

Należy napisać skrypt który:

a. przy pierwszym uruchomieniu wysyła na podany w pliku konfiguracyjnym adres
   e-mail informację o stanie parametrów komputera
b. przy kolejnych uruchomieniach najpierw sprawdza, czy którykolwiek parametr
   się zmienił, jeśli tak to wysyła ponownie e-maila ze wszystkimi parametrami,
   jeśli nie to nic nie robi

Parametry jakie sprawdza program:

* Adres IP komputera
* Ilość Ram (polecenie ``free``)
* Ilość procesorów (polecenie ``cat /proc/cpuinfo``).

Uruchamianie programów wielowątkowo
-----------------------------------

Mamy program który wykonuje obliczenia, program ten przyjmuje jako argument
plik z konfiguracją symulacji, plik ten wygląda tak::

  /emc/det/setMat  foo
  /emc/scorers/dump/trajectories 0

  /emc/initial_energy {EMC_ENERGY} MeV


  /random/setSeeds {{ e.random_x }} {{ e.random_y }}

  /run/beamOn 10000

Zadaniem skryptu jest:

1. Stworzenie 10 plików konfiuracyjnych w katalogu tymczasowym, w
   plikach tych ciągi znaków: {{ e.random_x }} {{ e.random_y }} zamieniane są
   na liczby losowe, a {EMC_ENERGY} na energie. Energia przyjmuje wartości:
   1, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000 MeV (inną w każdym pliku).

   .. note::

      Możesz spróbować: ``echo $RANDOM``.

2. Uruchomienie programu ``geant4sim.exe`` (w ramach
   ćwiczeń może on zawierać polecenie ``sleep 10``), program ten jest
   uruchamiany 10 razy (raz z każdym plikiem konfiguracyjnym), każdy
   program jest uruchomiony w oddzielnym wątku.
