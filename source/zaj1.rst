Zajęcia 1: Instalacja systemu debian
====================================


.. contents:: Spis treści

Filozofia Unixa
***************

.. epigraph::

     Do one thing and do it well - Write programs that do one thing and do it well.
     Write programs to work together. Write programs to handle text streams, because that is a universal interface.

     Everything is file - Ease of use and security is offered by treating hardware as a file.

     Small is beautiful.

     Store data and configuration in flat text files -
     Text file is a universal interface. Easy to create, backup and move to another system.

     Use shell scripts to increase leverage and portability -
     Use shell script to automate common tasks across various UNIX / Linux installations.

     Chain programs together to complete complex task -
     Use shell pipes and filters to chain small utilities that perform one task at time.

     Choose portability over efficiency.

     Keep it Simple, Stupid (KISS).


Instalacja systemu operacyjnego
*******************************

Kiedy wirtualizacja jest przydatna:

* Kiedy systemem matką jest Linuks, a trzeba pracować na Windowsie (i na odwrót!)
* Wirtualizacji używa się w dużych zespołach programistycznych --- każdy projekt
  ma dedykowany szablon maszyn wirtualnych, który jest skonfigurowany pod
  potrzeby tego projektu. W ten sposób programista może pracować nad dwoma
  projektami, które wymagają różnych konfiguracju systemu operacyjnego.
* Wirtualizacja jest używana przy wdrażaniu aplikacji. Na jednym serewerze
  może działać wiele aplikacji. Jeśli każda aplikacja jest
  na swojej maszynie wirtualnej wszystkie aplikacje są od siebie całkowicie
  odizolowane, mogą działać na różnych systemach operacyjnych, mieć przypisane
  inne adresy IP, można je oddzielnie włączać i wyłączać.


Stworzenie maszyny wirtualnej: 20 min
-------------------------------------

1. Włączenie kreatora tworzenia maszyn wirtualnych (``Machine`` -> ``New``)
2. Nazwa: Wasze imie i nazwisko (``jan-kowalski``)
3. Operating System Linux, Version Debian 64bit



   .. note::

        Konfigurujemy virtualboxa tak, że systemem gościem będzie GNU/Linux
        Debian w wersji o 64 bitowej architekturze procesora (chodzi tu
        o architekturę **maszyny gościa** można na 64bitowym hoście ustawić
        32 bitową maszynę wirtualną --- na odwrót nie jest to możliwe).

4. Memory: **ustaw 1GB**. Pozwala to określić ile z pamięci RAM systemu
   host jest widoczne dla maszyny wirtualnej. Ustawienie to można łatwo
   zmienić.
5. Stworzenie wirtualnego dysku: Create New

   .. note::

      Dyski maszyny wirtualej widzoczne są jako pliki na maszynie matce.
      Teraz konfigurujemy taki dysk.

 1. Rodzaj dysku: ``VDI``. Jest to natywny format dysków VirtualBoksa.
 2. Na następnej karcie: Dynamically allocated: w tym formacie rozmiar pliku z
    dyskiem rośnie w trakcie pracy.
 3. Położenie. Bez zmian. Domyślnie dysk zapisze się w katalogu ``~``
 4. Size: ``8GB``.

    .. note::

        Dla praktycznych zastosowań radzę wybrać większy dysk.
        Zmiana rozmiaru dysku po utworzeniu jest nietrywialna.

Po stworzeniu maszyny wirtualnej, należy umieścić obraz płyty w wirtualnym
napędzie CD. By to zrobić należy wybrać ``Settings``, dalej ``Storage``
kliknąć na ikonce dysku CD, wybrać: "Choose Virtual Optical Disk File",
następnie wybrać obraz z pliku ``/opt/sk/shared/iso/*iso`` (Obraz płyty
instalacyjnej debiana 8.1).

.. figure:: zaj1/open-cdrom.png

    Wybór płyty.


Instalacja w domu
-----------------

1. Virtualboxa można pobrać stąd: https://www.virtualbox.org/wiki/Downloads
2. Płytę instalacyjną Debiana stąd: http://cdimage.debian.org/debian-cd/8.2.0/amd64/iso-cd/debian-8.2.0-amd64-CD-1.iso

Instalacja systemu Debian: 45 min
---------------------------------

1. Język: Polski.
2. Kraj: Polska
3. Locale: ``en_US.utf8``
4. Hostname: ``debian``
5. Domain: ``dowolne``
6. Wybór hasła ``root``. Pozostawiamy puste

   .. note::

       Konto roota jest kontem administracyjnym zawierającym wszystkie uprawnienia.
       Dobrą praktyką bezpieczeństwa jest uniemożliwienie logowania na konto
       roota.

       Więcej w :doc:`zaj1/admin`

7. Użytkownik i hasło. Proszę wybrać swoje imie i nazwisko i hasło: ``student``.
8. Partycjonowanie dysków.

 .. note::

    Polecam zapoznanie się hierarchią systemu plików w systemie UNIX: :doc:`zaj1/fs`

 a. Wybieramy opcję Manual.
 b. Tworzymy dwie partycje:

   **Partycję na główny system plików**

   Parametry:

   1. Rozmiar 7GB
   2. Typ ``primary``
   3. Format ``ext4``
   4. Mount point ``/``


   .. note::

        W tej konfiguracji wszystkie dane będą na jednej partycji.

        W produkcyjnych środowiskach raczej przydziela się partycje na
        wybrane podkatalogi, na przykład ``/home`` jest na oddzielnej partycji.

   **Partycję na plik wymiany**

   1. Rozmair (reszta wolnej przestrzeni)
   2. Typ: ``Logical``
   3. Use as: ``swap``

   .. note::

        Obszar wymiany jest obszarem na dysku, do którego system operacyjnu może
        kopiować zawartość pamięci ``RAM``, gdy pamięci tej zbyt mało.

        W Windowsie plik wymiany jest plikiem na jednej z partycji, w
        UNIXach jest on z reguły oddzielną partycją.

 .. note::

    Uwaga: Jeśli instalujecie Debiana w domu, możecie spokojnie użyć 
    opcji "Guided".

9. Scan another CD: No
10. Use network mirror: yes, Poland, ``ftp.pl.debian.org``
11. Popularity contest: No
12. Software Selection:

    a. Debian desktop Enviorment
    b. Standard system utilities

13. Install GRUB to master boot record: wybieramy ``yes``. Następnie
    Wybieracie ``/dev/sda...``.

    .. note::

        Grub (Grand Unified Boot Loader) zarządza
        ładowaniem systemu operacyjnego (i pozwala wybrać odpowiedni
        jeśli jest kikla systemów do zastosowania).

Instalacja guest additions
--------------------------

Proszę wykonać (TODO):

.. code-block:: bash

    sudo aptitude install virtualbox-guest-utils



Ustawienia sieci w narzędziu VirtualBox
---------------------------------------

1. Wchodzimy w ustawienia maszyny wiertualnej (zaznaczyć i kliknąć settings).
2. Ustawienia dysków są pod ``Network``.
3. Nie dokonujemy zmian, ale warto wiedzieć jakie są możliwości.

Do maszyny wirtualnej można podpiąć 4 karty sieciowe. Każda z tych kart
może działać w takich trybach:

.. warning::

    Opisy te mogą się wydać enigmatyczne, ale przy odrobinie szczęścia,
    po kolejnych wykładach będą jasne.

``NAT``

    Maszyna dziecko funkcjonuje w wirtualnej podsieci, w której rolę
    bramy pełni maszyna matka. Można skonfigurować przekierowania portów,
    tak, by połączenie do maszyny matki na port X było przekierowane
    na daną maszynę wirtualną.

    Jest to najprostsze rozwiązanie.

``Host-only-network``

    Dana karta sieciowa jest wpięta w wirtualną sieć na komputerze matce,
    ma w tej sieci dostęp do:

    * Wszystkich uruchomionych maszyn wirtualnych
    * Maszyny matki

    Dane z tej sieci nie wchodzą na zewnątrz maszyny matki.

    Warto skorzystać z takiej sieci jeśli:

    * Dane, które wynieniają między sobą maszyny wirtualne są poufne.
    * Jeśli chcemy mieć pełną kontrolę nad adresami IP maszyn w wirtualnej
      sieci.

``Bridged``

    Dana karta jest podpięta do wybranego interfejsu sieciowego na maszynie
    matce (np. połączenie kablowe ``eth0``, połączenie wifi ``wlan0``).

    Maszyna będzie funkcjonować jako kolejny host działający bezpośrednio
    w danej sieci. Gdybyśmy wybrali tą opcję, nasza maszyna byłaby widoczna
    w sieci Wydziału Fizyki.

    Warto skorzystać z tej sieci jeśli:

    * Nasza maszyna wirtualna ma być widoczna w sieci maszyny matki
      (jest na przykład serwerem).


Powłoka bash
************

Podstawy bash: 10min
--------------------


Polecenie ``echo`` jest bardzo proste: po prostu wyświetla to, co
otrzymało z linii komend.

.. code-block:: bash

    echo foo
    foo
    cd /
    echo *
    bin boot dev etc home initrd.img initrd.img.old lib lib32 lib64 lost+found media mnt opt optl proc root run sbin selinux srv sys tmp usr var vmlinuz vmlinuz.old
    echo b*
    bin boot
    echo ~
    /home/jb

Jeśli polecenie ``echo`` po prostu wyświetla na standardowe wyjście to, co zostalo mu
podane w lini komend, to czemu w powyższym przykładzie echo nie wyświetliło ``*``?
Otóż powłoka bash, automatycznie dokonuje pewnych konwersjii na linii poleceń przed
uruchomieniem
programu. Najprostszą z nich jest rozwijanie metaznaków. Przykładowo ``*``
zastępuje dowolny ciąg znaków, a ``~`` jest zastępowana przez katalog domowy
użytkowika.

Ćwiczenie 1
^^^^^^^^^^^

Za pomocą polecenia ``echo`` wyświetlić wszystkie pliki kończące się na ``.conf``
z katalogu ``/etc``

Ćwiczenie 2
^^^^^^^^^^^

Proszę pobrać tekst Romeo i Julii po angielsku::

    wget http://db.fizyka.pw.edu.pl/sk-2015/_downloads/pg1112.txt

Sprawdzić, w którym jesteśmy katalogu (``pwd``).

Stworzyć w katalogu ``/tmp``, katalogi  ``foo`` i ``bar`` (polecenie ``mkdir``).

Skopiować do katalogu ``foo`` plik z tekstem dramatu.

Przejść do katalogu ``bar`` i wyświetlić zawartość dramatu za pomocą ścieżki
absolutnej.

Skopiować dramat z katalogu ``foo`` do ``bar`` za pomocą ścieżki względnej.

Powłoka Bash: przekierowania 10min
----------------------------------

Program uniksowy może przyjmować dane z takich źródeł:

* Z linii komend
* Ze standardowego wejścia (tzw. konsola)
* Może odczytywać pliki (w tym pliki konfigurecyjne)
* Ze środowiska (odczyt zmiennych środowiskowych)

Program może wysyłać dane w takie miejsca:

* Na standardowe wyjście
* Może zapisywać pliki

.. figure:: zaj1/shell-program.*

    Schemat działania programu.

Filtr
^^^^^

Filtr to specjalny rodzaj programu linuskowego, który
pobiera dane (tylko) ze standardowego wejśca i
zapisuje je (tylko) na standardowe wyjście.

.. figure:: zaj1/filter.*

    Schemat działania filtru.


Podstawy słownika
^^^^^^^^^^^^^^^^^

Program ``cat`` odczytuje zawartość plików i
przekierowuje ją na standardowe wyjście:

.. code-block:: bash

    cat nazwa_pliku

By przekierować standardowe wyjście programu do pliku należy
użyć operatora ``>`` lub ``>>``.


.. code-block:: bash

    cat plik1 plik2 >> polaczonepliki


W wariancie z `>` polecenie to spowoduje nadpisanie
pliku zawartością standardowego wyjścia, w wariancie `>>`
spowoduje dopisanie treści na końcu pliku.

By uruchomić ``polecenieB`` i podać mu na standardowe wejście
standardowe wyjście polecenia ``polecenieA`` należy napisać:

.. code-block:: bash

    polecenieA | polecenieB


Inne filtry:


``tail``

    Pokazuje koncówkę pliku

    .. code-block:: bash

        cat /usr/share/romeo > tail -n 100

``head``

    Pokazuje początek pliku

Przekierowanie standardowego wyjścia i standardowego strumienia błędów
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Każdy program ma podłączone dwa standardowe strumienie wyjściowe:

* Standardowe wyjście
* Standardowy strumień błędów

Są one odróżnione by np. polecenie tar mogło na standardwe wyjście zwracać 
spakowany katalog, a na standardowy strumień blędów zwracać informacie o błędach.

By przekierować standardowy strumień błędów należy użyć ``2>`` lub ``2>>``.

Ćwiczenie 1: Powłoka bash: przekierowania
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Proszę pobrać tekst Romeo i Julii po angielsku :download:`/downloads/pg1112.txt`

.. code-block:: bash

    wget http://www.gutenberg.org/cache/epub/1112/pg1112.txt

Po kolei należy:

1. Wylistować linie zawierające słowo ``Juliet``.
2. Wylistować linie zawierające słowo ``Juliet`` z numerami linii.
3. Wylistować linie zawierające na raz słowa ``Juliet`` i ``Romeo`` oraz zapisać do pliku.


Ćwiczenie 2: Polecenie ``cut``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Proszę wylistować użytkowników z pliku ``/etc/passwd`` za pomocą
polecenia ``cut``.

.. note::

  Polecenie ``cut`` służy do wycinania elementów z linii, ma ono sporo
  możliwości, ale w waszym przypadku starczą dwie opcje: ``-d :`` powoduje że
  ``cut`` dzieli linie w miejscach, w których pojawia się znak ``:`` oraz
  ``-f N``, który wybiera pole ``N`` z pliku.

Ćwiczenie 2: Kompresja katalogu za pomocą ``tar`` i ``bzip2``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Proszę skompresować wybrany katalog za pomocą poleceń ``tar`` i ``bzip2``,
proszę zapisać polecenie w jednej lini (bez tworzenia pliku ``*tar``).

.. note::

    Do spakowania zawartości katalogu potrzebne są dwa polecenia:
    ``tar`` i ``bzip2``. ``tar`` potrafi zapisać drzewo plików i katalogów do
    pojedynczego pliku, ``bzip2`` potrafi natomiast spakować jeden plik (formalnie jeden strumień).

    By spakować katalog należy wykonać:

    .. code-block:: bash

         tar -c /katalog >  /tmp/spakowane.tar


    By skompresować plik należy wykonać:

    .. code-block:: bash

        cat plik | bzip2 -c > plik.bz2

    Waszym zadaniem jest skompresowanie katalogu (może być to ``/tmp/foo`` --
    utworzony w jednym z poprzednich zadań) do pliku, **bez tworzenia
    pliku tar**.

    .. note::

        W zasadzie możecie spytać czemu po prostu nie użyć formatu ``zip``,
        albo w czym format ``zip`` jest gorszy od ``tar.bz2``.

        Odpowiedź jest filozoficzna: otóż filozofia linuksa zakłada, że mamy
        jeden program do jednego celu, program ``zip`` robi wiele rzeczy na raz:

        * Pakuje wiele plików w jeden format
        * Kompresuje pliki
        * Ma wsparcie do szyrfowania

        Może być to odrobinę wygodniejsze, ale kosztem mniejszej elastyczności,
        bardzo łatwo spakować plik ``tar`` za pomocą polecenia ``xz``, które
        kompresuje lepiej niż ``zip`` i ``bzip2`` (kosztem znacznie zwiększonego
        zużycia procesora), format ``zip`` zostaje natomiast z jednym
        algorytmem kompresii. Tak samo łatwo zaszyfrować taki plik, format
        zip wspiera szyfrowanie (ale stare jego wersje nie kompresują w sposób
        bezpieczny``.


Specjalne pliki 5min
--------------------

``/dev/null``
^^^^^^^^^^^^^

Plik ``/dev/null`` to plik, który przyjmuje dowolne dane, które potem znikają.

Jest on przydatny kiedy chcemy się pozbyć jakiegoś strumienia danych.

Kiedy wyszukiwaliście Państwo plików często polecenie ``find`` wyświetlało
informacje o błędach, potoku zawierającego te informacje można łatwo się pozbyć.

.. code-block:: bash

    find / "pattern" 2> /dev/null

pozwala pozbyć się informacji o błędzie.

``/dev/[u]random``
^^^^^^^^^^^^^^^^^^

Plik ``/dev/random`` oraz ``/dev/urandom`` zawierają nieskończone ciągi losowych
liczb.

Taka komenda pozwala na wpipsanie kilku losowych znaków::

    cat /dev/urandom | head -c 66 | base64


Sygnały
-------

Sygnały są metodą na komunikację między procesami w systemach UNIXowych.
Pozwalają, na przykład, na:

* Poproszenie procesu o wyłączenie się
* Wymuszenie wyłączenia procesu
* Zatrzymanie wykonania procesu

Do wysyłania sygnałów służą polecenia oraz ``pkill``, ``kill``.

Polecenie ``kill`` przyjmuje ``id`` procesu, polecenie ``pkill`` wysyła sygnał
do procesów, których nazwa zawiera ciąg znaków podanych w tym poleceniu.

.. code-block:: bash

    jb /tmp/bar $ ps -e | grep iceweasel
    5020 ?        00:13:16 iceweasel
    kill 5020

To samo zadanie wykona:

.. code-block:: bash

    pkill iceweasel

Domyślnie ``pkill`` i ``kill`` wysyłają sygnał ``TERM``, który prosi proces
by ten się wyłączył.

Polecenia ``pkill -9`` oraz ``kill -9`` pozwalają na zabicie procesu.

Polecenie ``pkill --signal SIGSTOP`` spowoduje wstrzymanie wykonania procesu,
a ``pkill --signal SIGCONT`` włączy go ponownie.

Ćwiczenie 1
^^^^^^^^^^^

Proszę uruchomić przeglądarkę ``iceweasel``, wstrzymać ją a następnie
wznowić jej proces.

Zadanie końcowe
---------------

W katalogu ``/proc`` zawarte są dane o wszystkich procesach w linuksie,
w katalogu ``/proc/1234`` są dane o procesie o ``id`` równym ``1234``, w tym
katalogu jest plik status zawierający informacje o procesie.

.. note::

    Opis zawartości ``/proc`` można poznać wpisując:

    .. code-block::

        man 5 proc

Plik status ma zawartość podobną do:

.. code-block:: bash

    Name:   cat
    State:  R (running)
    Tgid:   5556
    Pid:    5556
    PPid:   5555
    TracerPid:      0
    Uid:    0       0       0       0
    Gid:    0       0       0       0
    FDSize: 64
    Groups: 0
    VmPeak:     5824 kB
    VmSize:     5824 kB
    VmLck:         0 kB
    VmPin:         0 kB
    VmHWM:       360 kB
    VmRSS:       360 kB
    VmData:      176 kB
    VmStk:       136 kB
    VmExe:        44 kB
    VmLib:      1808 kB
    VmPTE:        28 kB
    VmSwap:        0 kB
    Threads:        1
    SigQ:   1/125663
    SigPnd: 0000000000000000
    ShdPnd: 0000000000000000
    SigBlk: 0000000000000000
    SigIgn: 0000000000000000
    SigCgt: 0000000000000000
    CapInh: 0000000000000000
    CapPrm: 0000001fffffffff
    CapEff: 0000001fffffffff
    CapBnd: 0000001fffffffff
    Seccomp:        0
    Cpus_allowed:   ff
    Cpus_allowed_list:      0-7
    Mems_allowed:   00000000,00000001
    Mems_allowed_list:      0
    voluntary_ctxt_switches:        0
    nonvoluntary_ctxt_switches:     1

Nas interesuje pole ``VmSize`` zawierające rozmiar pamięci używanej przez program
proszę napisać linijkę kodu, która będzie wyświetlać rozmiar pamięci zajmowanej
przez program, tj. dla procesu z przykładu wyświetli:

.. code-block:: bash

    5824 kB

Wskazówki:

1. Należy odczytać ten plik
2. Znaleźć linijkę zawierającą ``VmSize`` (``grep``).
3. Za pomocą polecenia ``cut`` wyciąć z niej wszystko co znajduje się przed ``:``.


Przydatne polecenia
-------------------

``echo``

    Zwraca linie poleceń na standardowe wyjście

    .. code-block:: bash

        echo foo bar baz;


``cd``
    Przenosi do innego katalogu.

    .. code-block:: bash

        cd /tmp;
        cd ; # Przenosi do katalogu domowego
        cd ~/foobar; # przenosi do katalogu foobar w katalogu domowyn

``ls``

    Listuje zawartość katalogu

    .. code-block:: bash

        ls ;
        ls -al; #listuje wszystkie pliki z dodatkowymi informacjami
        ls -a1; #wszystkie pliki jeden na linię (do skryptów)
        ls /tmp; # listuje katalog tmo


``mkdir``

    Tworzy katalog

    .. code-block:: bash

        mkdir /tmp/foo


``cat``

    Otwiera wiele plików i przekierowuje je na standardowe wyjście

    .. code-block:: bash

        cat /usr/share/romeoandjuliet;
        cat *sql;

``tail``

    Pokazuje koncówkę pliku

    .. code-block:: bash

        tail /usr/share/romeoandjuliet;
        tail -n 1000 /usr/share/romeoandjuliet;# Pokazuje ostatnich 1000 linii
        tail -f /usr/share/romeoandjuliet; #Nasłuchuje nowych zmian w pliku

``cp``

    Kopiuje katalogi i pliki

    .. code-block:: bash

        cp plik1 plik2; #kopiuj plik1 na plik2
        cp plik kat; #kopiuj plik1 do katalogu kat
        cp kat1 kat2; #kopiuj katalog kat1 do katalogu kat2
        cp -a kat1 kat2; #kopiuj katalog kat1 do katalogu kat2 (z zachowaniem praw)

``scp``

    Kopiuje pliki z komputera na komputer za pomocą protokołu ``ssh``.

    .. code-block:: bash

        scp -r build/html lfitj.if.pw.edu.pl:/var/www/dydaktyka/bd
        scp -R /tmp/foo foo@bar:/tmp/baz # Kopiuje katalog /tmp/foo na server bar do katalogu /tmp/baz przy czym na zdalny serwer logujemy się na użytkownika foo

``rsync``

    Synchronizuje dwa katalogi na dwóch komputerach oszczędzając transfer
    (przesyła tylko zmiany w plikach).

``ssh``

    Pozwala na zdalne zalogowanie na inny komputer

``mv``

    Przenosi katalogi

    .. code-block:: bash

        mv plik1 plik2; #przenieś plik1 na plik2 (zmiana nazwy pliku)
        mv plik kat; #przenieś plik1 do katalogu kat
        mv kat1 kat2; #przenieś katalog kat1 do katalogu kat2


``rm``

    Usuwa pliki

    .. code-block:: bash

        rm plik1 plik2;# usuń plik1 i plik2
        rm *~;# usuń wszystkie pliki kończące się na ~ w bieżącym katalogu
        rm -r kat; #usuń katalog kat wraz z podkatalogami
        rm -rf kat; #usuń katalog kat wraz z podkatalogami (w trybie force)
        rm -ri kat; #usuń katalog kat wraz z podkatalogami (w trybie interaktywnym)


``less``

    Odczytuje plik strona po stronie

    .. code-block:: bash

        less /usr/share/romeoandjuliet;

``file``

    Zgaduje typ pliki za pomocą `magicznych numerów <http://en.wikipedia.org/wiki/Magic_number_(programming)>`_,
    czyli specjalnych ciągów bajtów znajdującyh się w pliku, które pozwalają
    określić rodzaj jego zawartości. Na przykład pliki, które rozpoczynają się
    od dwóch bajtów: 035 i 033, są plikami skryptów (jest tak ponieważ w
    kodowaniu ``ASCII1`` ciąg ``#!`` ma taką reprezentację. Ciąg ten stanowi
    początek :ref:``shebang-line``.

    .. code-block:: bash

        jb@gautama:/tmp$ file Dredmor-amd64
        Dredmor-amd64: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.32, not stripped
        jb@gautama:/tmp$ file foo.xlsx
        foo: Microsoft Excel 2007+
        jb@gautama:/tmp$ file cyclotron.jar
        cyclotron.jar: Zip archive data, at least v2.0 to extract

``find``

    Wyszukuje pliki.

    .. code-block:: bash

        find -name "*odt"; # Uwaga " są konieczne!

``grep``

    Wyszukuje linie w pliku

    .. code-block:: bash

        grep Romeo /usr/share/romeoandjuliet # Wyszukuje linie zawierające słowo "Romeo"


``whereis``

    Znajduje położenie pliku o ile jest on w $PATH

    .. code-block:: bash

        whereis java; #Znajdzie położenie programu java


``nano``

    Konsolowy edytor tekstowy

    .. code-block:: bash

        nano /etc/X11/xorg.conf;


``traceroute``

    Pokazuje ścieżkę do zdalnego serwera

    .. code-block:: bash

        traceroute google.pl

``ping``

    Bada czas odpowiedzi do danego hosta. Przydatne do sprawdzania
    istnienia i jakości połączenia internetowego.

    .. code-block:: bash

        ping google.pl


``chown``

    Zmienia właśniciela i grupy pliku

    .. code-block:: bash

        chown foo:bar baz; # Zmienia właściciela baz na foo a grupy na bar
        chown -R baz ; # j.w. tylko zmienia właściciela wszystkich podkatalogów baz


``chmod``

    Zmienia uprawnienia pliku i katalogu

    .. code-block:: bash

        chmod og-rxw foo ; #Odbiera wszystkie uprawnienia do foo wszystkim poza właścicielem
        chmod 700 foo ; #j.w.
        chmod -R 700 /tmp/foo ; #j.w. tylko zmienia uprawnienia w podkatalogach i plikach

``pwd``

    Wyświetla bieżący katalog

    .. code-block:: bash

        pwd


``ps``

    Listuje procesy

    .. code-block:: bash

        ps; #pokaż moje procesy, widok podstawowy
        ps -u user; #pokaż procesy użytkownika, widok podstawowy
        ps -aux;# pokaż wszystkie procesy, widok rozszerzony


``top``

    Listuje procesy (interaktywne)

    .. code-block:: bash

        top


``kill``

    Wysyła sygnał do procesu, za pomocą numeru procesu

    .. code-block:: bash

        kill 1111# Prosi proces o wyłączenie się
        kill -9 1111# Zabija proces
        kill -22 1111 #Zatrzymuje proces (może być potem wznowiony)


``pkill``

    Wysyła sygnał do procesu, wyszukując procesy po nazwie

    .. code-block:: bash

        pkill firefox; #Zabija wszystkie procesy o nazwie firefox
        pkill -9 firefox;
        pkill -f netbeans; #Zabija procesy w których poleceniu pojawia się słowo netbeans


``xkill``

    Pozwala usunąć program po kliknięciu na nim

    .. code-block:: bash

        xkill


``wget``

    Pobiera plik za pomocą protokołu HTTP

        wget google.pl


Dodatkowe materiały
*******************

.. toctree::
   :maxdepth: 1
   :glob:

   /zaj1/*


