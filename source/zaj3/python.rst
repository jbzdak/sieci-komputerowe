Gniazda sieciowe w języku python
================================

Wstęp
-----

.. note::

    Python jest łatwym do nauki, prostym językiem programowania ogólnego
    przeznaczenja (tj. nie jest on dedykowany do jednego rodzaju aplikacji).



Gniazda sieciowe w Pythonie oprogramowuje się tak samo jak w C (większość wywołań
funkcji ma odwzorowanie jeden do jeden na funkcje z C.

Nawiązanie połączenia
---------------------

.. code-block:: py

    # -*- coding: utf-8 -*-

    import socket

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("google.pl", 80))
    s.send("GET / HTTP/1.0\n\n")
    data = s.recv(1000)

    print(data)
    print("Reclieved {} bytes".format(len(data)))


Opisy działania funkcji ``socket``, ``connect``, ``send`` w zasadzie pokrywają
się z opisami tych funkcji w części o C.

Odebranie połączenia
--------------------

.. code-block:: py

    # -*- coding: utf-8 -*-

    import socket

    HOST = 'localhost'
    PORT = 5555
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT)) #Uwaga -- te nawiasy są ważne, określaja pythonową krotkę (touple)
    s.listen(1)
    conn, addr = s.accept()
    print('Connected by', addr)
    while 1:
        data = conn.recv(1024)
        if not data: break
        conn.sendall(data)
    conn.close()

Opisy działania funkcji ``socket``, ``listen``, ``bind`` w zasadzie pokrywają
się z opisami tych funkcji w części o C.

Serwer wielowątkowy
-------------------

.. code-block:: py

    # -*- coding: utf-8 -*-
    import multiprocessing

    import socket
    import threading

    HOST = 'localhost'
    PORT = 5555
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)

    def handle(conn, add):
        while 1:
            data = conn.recv(1024)
            if not data: break
            conn.sendall(data)
        conn.close()

    while True:
        conn, addr = s.accept()
        print('Connected by', addr)
        t = multiprocessing.Process(target=handle, args=(conn,addr))
        t.start()

ten przykład jest w zasadzie taki sam jak dla Javy i C.

Funkcja ``handle`` będzie obsługiwać połączenie w oddzielnym procesie.

Linijka ``multiprocessing.Process(target=handle, args=(conn,addr))`` tworzy nowy
proces, który będzie wykonywał funkcję ``handle`` wywołaną z parametrami
``conn,addr``.

Linijka ``t.start`` uruchamia proces.