Ogólne rozważania
=================

Blokująca komunikacja
---------------------

Podane metody komunikacji (we wszystkich językach) są **blokujące**, tj.
metoda kończy się wykonywać gdy otrzyma dostateczną ilość danych:

* ``accept`` --- gdy przyjdzie jakieś połączenie
* ``recv``, ``bufferedReader.readLine()`` --- gdy bufor się wypełni, lub
  druga strona kończy nadawać.
* ``send`` --- aż wysyłka danych się nie skończy

Powoduje to jeden problem --- kiedy odczytuje się dane, klient lub serwer
mogą się zablokować ponieważ druga strona wysłała albo za mało danych,
albo nie wysłała znacznika końca wiadomości.

Znacznik końca wiadomości
-------------------------

Ważnym problemem jest dzielenie strumienia na poszczególne wiadomości. W większości
przypadkóœ w przykładach tym co oddzielało wiadomości był znak końca linii.

Możliwe są inne schematy dzielenia wiadomości.

Network byte order
------------------

Typy stałoprzecinkowe (integer, long) zajmują kilka bajtów, procesory mogą 
być przystosowane do operacji na tych liczbach zgodnie z dwoma kolejnościami,
little endian oraz big endian.

Przykładowo liczba (integer 1) może w pamięci być zapisany jako czery bajty:
``0, 0, 0, 1`` albo ``1, 0, 0, 0``, w zależności od tego jakiej kolejności
oczekuje procesor danej maszyny.

Podczas pracy na jednym komputerze nie jest to problem, natomiast
może być to problem kiedy wysyłamy dane w sieci, między systemami z których
jeden jest big endian a drugi little endian.

W tym celu powstało coś co nazywa się *Network byte order*, czyli kolejność
bajtów kiedy podróżują one po sieci.

Do konwersji między kolejnością sieciową, a kolejnością maszyny służą odpowiednie funkcje.

W ``C/C++`` do tej konwersji służy zestaw funkcji: ``htons``, ``htonl``, ``ntohs``...
(więcej w `man 3 byteoder <http://linux.die.net/man/3/byteorder>`_). Przykładowo
htonl konwertuje typ ``long`` z kolejności komputera do kolejności sieciowej.

W pythonie są to funkcje z modułu ``socket`` (o takich samych nazwa).

W ``Javie`` jest to mniejszy problem, ponieważ maszyna wirtualna Java
używa zawsze kolejności zgodnej z network byte order.



