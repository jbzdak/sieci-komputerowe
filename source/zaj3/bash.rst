
Programowanie gniazd sieciowych w BASH
--------------------------------------

Wbrew pozorom w bashu można dość łatwo tworzyć rozwiązania sieciowe,
rozważmy takie dwa programy:

Pierwszy nazywa się ``server-simple.sh``

.. code-block:: bash

    #!/bin/bash

    FILE="/tmp/chatroom-simple"

    function read_message {
        __IFS=$IFS
        IFS=
        read message
        IFS=${__IFS}
        echo ${message}
    }

    echo "submit your message"
    message=$(read_message)
    echo "${message}" >> ${FILE}

.. note::

    polecenie ``read`` odczytuje wartość jednej zmiennej z standardowego wejścia,
    domyślnie zmienne separowane są spacjami, zmienna ``IFS`` pozwala kontrolować
    jak bash tokenizuje ciągi znaków.

    Ustawienie ``IFS=`` powoduje że polecenie ``read`` odczyta całe standardowe
    wejście.

Program ten odczytuje linijkę tekstu i zapisuje ją do pliku.

Drugi nazywa się ``server-read-simple.sh``

.. code-block:: bash

    #!/bin/bash

    FILE="/tmp/chatroom-simple"

    cat $FILE

Skrypt ten odczytuje plik z dysku i go wyświetla.

Proszę zauważyć że dwóch użytkowników, może za pomocą tych dwóch programów
chatować (o ile obaj użytkownicy działają na jednym komputerze).

Teraz chcielibyśmy udostępnić te dwa programy jako usługi sieciowe
za pomocą TCP.


BASH --- serwer
***************

Programistyczne powinno być to całkiem łatwe, te programy przecież odczytują 
i zapisują dane na standardowe wejście i wyjście, które są strumieniami,
chciałoby się po prostu *przekierować* standardowe wejście i wyjście na
odpowiednie połączenie tcp.

Do stworzenia takiego serwera służy polecenie ``ncat``.

Przykładowo::

    ncat -l 8000 -k -e server-simple.sh

tworzy serwer na porcie ``8000``, serwer ten dla każdego przychodzącego połączenia
wykonuje skrypt ``server-simple.sh``.

Oraz::

    ncat -l 8001 -k -e server-read-simple.sh

tworzy serwer na porcie ``8001``, serwer ten dla każdego przychodzącego połączenia
wykonuje skrypt ``server-read-simple.sh``.

Możemy sprawdzić ich działąnie za pomocą połączenia telnet. Właśnie stworzyliśmy
bardzo prosty serwer chatu.


Narzędzie ``telnet``
********************

Telnet jest narzędziem pozwalającym wykonywać połączenia TCP z serwerem.

Mozecie Państwo użyć go by spróbować wykonać połączenie do właśnie stworzeonych
serwerów.


BASH --- klient
***************

Teraz chcielibyśmy opracować klienta, tak bym mógł napisać ``./list-messages.sh``
i otrzymać listę wiadomości.

W linuksach istnieje magiczna struktura ``/dev/tcp/host/port``, zawierająca
pliki pozwalające na wykonywanie połączeń TCP.

.. note::

    Do tej pory mówiliśmy że program ma trzy przypisane do niego strumienie
    (standardowe wejście, standardowe wyjście oraz standardowe wyjście błędu).

    Takich strumieni może być dowolnie wiele. Możemy sami je definiować.

    Polecenie ``exec 5<> NAZWA_PLIKU`` spowoduje że do wejścia numer 5 zostanie
    podłączony plik ``NAZWA_PLIKU`` (zawartość pliku zostanie tam przesłana)
    a dane wysłane na wyjścia numer 5 trafią do tego pliku.

.. code-block:: bash

    # Ustawiamy że para wejście i wyjście numer 5 będzie przypięte do
    # połączenia TCP z hostem google.pl na porcie 80
    exec 5<>/dev/tcp/google.pl/80
    # Wysyłamy zapytanie HTTP
    echo -e "GET / HTTP/1.0\n" >&5
    # Wyświetlamy to co serwer nam odpowiedział.
    cat <&5

W taki sam sposób mogę oprogramować klienta naszego chatu.

BASH --- zdanie
***************

Proszę postarać się do końca zajęć napisać dwa programy:

* ``simple-list.sh``, który łączy się z serwerem i pobiera listę wiadomości.
* ``simple-post.sh "moja wiadomość"``, który łączy się z serwerem i ustawia wiadomość.

