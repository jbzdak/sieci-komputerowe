Protokół TCP
============

Protokół ``TCP`` jest jedną z możliwych implementacji
warstwy transportu w modelu ``OSI``, ma on następujące cechy:

jeden-do-jeden

    Pozwala na uwtorzenie dwukierunkowego kanału danych między dwoma hostami
    w sieci.

niezawodność (*reliability*)

    Dane wysłane za pomocą protokołu ``TCP`` muszą dotrzeć do systemu docelowego.
    Jeśli nie jest możliwe dostarczenie danych (np. z powodu braku
    łączności z internetem systemu docelowego) implementacja protokołu
    TCP musi zwrócić błąd.

kontrolę błędów

   Dane wysłane przez protokół ``TCP`` posiadają sumy kontrolne, które
   pozwalają wykryć błędy w transmisji.

kolejność

    Jeśli jakaś wiadomość TCP jest wysłana jako wiele oddzielnych pakietów
    IP, wiadomość dotrze w tej samej kolejności w jakiej była wysłana.

Implementacja TCP
-----------------

Na poziomie protokołu IP operuje się pojedyńczymi pakietami,
pakiety te mogą po drodze między dwoma systemami *się zawieruszyć*.

Na poziomie protokolu TCP operuje się *strumiueniami danych*, strumień tych
danych jest niezawodny.

W praktyce programowanie oparte na strumieniach (znane z basha) i programowanie
TCP nie różni się zbytnio.

Pojęcie portu
-------------

Adresy IP jednoznacznie idenyfikują komputery, my chcielibyśmy jeszcze móc
zaadresować kilka usług na jednym komputerze (jest to nie możliwe
na poziomie protokołu IP!). Dlatego ``TCP`` (oraz ``UDP``) używają 
dodatkowego numeru zwanego numerem portu.

Na komputerze docelowym numer portu jest najczęściej powiązany z usługą 
z jaką komputer chce się połączyć (port 80 to HTTP) -- pełna lista portów
dostępna jest `w Internecie
<http://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers#Well-known_ports>`_.

Na komputerze inicjującym port jest wybierany losowo.

Handshake TCP
-------------

Połączenie TCP jest jednoznacznie identyfikowane przez cztery liczby:


``destination-port``

    Numer portu na komputerze do którego jest nawiązywane połączenie.

``destination-host``

    Numer IP hosta do którego jest nawiązywane połączenie.

``source-port``

    Numer portu na komputerze z którego jest nawiązywane połączenie.

``source-host``

     Numer IP hosta z którego jest nawiązywane połączenie.


By nawiązać połączenie należy wymienić tzw. handshake TCP, polega on na wysłaniu
trzech pakietów:

``SYN``

   Wysyła go klient, zawiera on losową liczbę x.

``SYN+ACK``

   Wysyła go serwer, zawiera on losową liczbę y, oraz liczbę x+1.

``ACK``

    Wysyła go klient zawiera on liczbę y+1.

Narzut TCP
----------

Użycie protokółu TCP powoduje powstanie pewnego narzutu, którego oszacowanie
jest trudny, a sam rozmiar narzutu zależny od rozmiaru poszczególnych pakietów.

Dla długich połączeń narzut TCP jest w granicach 5%, dla wysłania pojedyńczej
wiadomości może wynosić kilkaset procent (koszt wykonania handshake).
