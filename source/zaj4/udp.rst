UDP
===

UDP (User datagram protocol) jest protokołem który ma następujące własności:

* Jest bezstanowy (tj. do wysłania komunikatu nie trzeba nawiązywać połączenia)
* Pakietowy (wysyłamy pakiety)
* Nie zapewnia retransmisji danych
* Opcjonalnie: zawiera sumę kontrolną 
* Umożliwia wysyłanie danych do wielu użytkowników

Zastosowania UDP
----------------

Wysyłanie danych do wielu użytkowników

    Na przykład synchronizacja plików między wieloma serwerami w jednej sieci.
    Jeden z serwerów wysyła dane za pomocą pakietów broadcast (trafiają one
    do wszystkich systemów w sieci)
    `przykładowe rozwiązanie <https://www.udpcast.linux.lu/>`_
    (nie sprawdzałem!).

Usługi zero-konfiguracji

    Do tej pory wszystkie usługi które pisaliśmy wymagały posiadania numeru IP
    serwera. Jest to całkiem uciążliwe w małych sieciach i sieciach ad-hoc.

    Chcielimbyśmy by nasza sieć działała tak że po podłączeniu nasz system
    może wykryć wszystkie usługi widoczne w sieci.

    Tego typu rozwiązania często implementuje się za pomocą UDP.

    Przykładem może być usługa NetBIOS, będąca częścią systemu sieciowego
    systemu Microsoft Winows (zaimplementowanego w linuksie jako SAMBA).

Sieci peer-to-peer

    Część sieci peer-to-peer korzysta z UDP zamiast z TCP ponieważ nie potrzebują
    obsługi błędów (same posiadają sumy kontrolne poszczególnych plików).

Wysyłanie wielu małych pakietów

    Kiedy nasz system wysyla wiele krótkich wiadomości do wielu innych systemów,
    okazuje się że narzut na handshake TCP jest nie do dość wydajne.

    Przykładowo protokół ustalania czasu (Network Time Protocol) korzysta z
    UDP do komunikacji z klientami.

Rozwiązania wymagające niskich opóźnień

    W przypadku streamingu mediów (audio, video), czy oprogramowania gier
    sieciowych ważne jes to by opóźnienie przesłanych informacji było minimalne.


Broadcast
---------

Broadcast jest w zasadzie funkcjonalnością protokołu ``IP``, ale skoro nie można
z niego skorzystać z poziomu ``TCP`` to mówimy o nim dopiero teraz.

Załóżmy że mamy podsieć o masce: 255.255.254.0 i adresie ip 194.29.174.123, to jeśli
wyślemy pakiet na adres 194.29.175.255 (adres w którym wszystkie miejsa w których
maska przyjmuje wartość ``0`` są zamienione na ``1``) wiadomość taka zostanie
wysłana do wszystkich komputerów w danej podsieci.

Maska podsieci::

   11111111 11111111 11111110 00000000

Adres komputera::

   11000010 00011101 10101110 01111011

Adres Broadcast::

   11000010 00011101 10101111 11111111

Multicast
---------

Przesyłanie informacji od wszystkich użytkowników danej podsieci ma wiele
zastosowań, ale ma poważne ograniczenia --- wiadomości broadcast generalnie
są wycinane przez routery, i nie nadają się do komunikacji poza siecią 
lokalną.

Rozważmy przykładowo telewizję internetową: jeśli korzysta z wiadomości unicast
(do jednego odbiorcy) ilość pakietów które wysyła rośnie liniowo z ilością 
odbiorców --- mimo że każdy odbiorca dostaje taki sam obraz (a w zasadzie:
taką samą listę pakietów).

By rozwiązać takie problemy stworzono wiadomości ``multicast``, działają one w
następujący sposób:

Zdefiniowano podsieć ``224.0.0.0/4`` jako podsieć zawierającą adresy multicast,
adresy z tego zakresu te są przydzielane przez organizację IANA. Adresy te
jednak nie oznaczają żadnych *hostów*, ale *grupy* multicast. Poszczególne
komputery mogą wyrazić zainteresowanie uczestniczeniem w danej grupie multicast,
informację tą wysyłają do swojego najbliższego routera (który, jeśli to konieczne
przesyła ową informację dalej). Od tej chwili pakiety wysłane na dany adres
multicast będą przesyłane również do komputera który zarejestrował się
w do danej grupy.