UDP w Bashu
===========

Klient
------

W przypadku łączenia się z pojedyńczym serwerem należy można użyć filesystemu
``/dev/udp/host/port``. W przypadku wysylania broadcastów można skorzystać 
z ``echo "wiadomosc" | ncat -u 192.168.1.255 8000``.

Serwer
------

Jak w przypadku ``TCP`` pomóc może polecenie ncat.

Przykład
--------

.. code-block:: bash

    #!/bin/bash
    __IFS=$IFS
    IFS=
    read message
    IFS=${__IFS}

    echo ${message} >&2 #Wyslanie wiadomosci na konsole serwera
    echo "OK" #Odpowiedz do klienta ze otrzymal wiadomosc

Uruchamiamy za pomocą:

.. code-block:: bash

    ncat -l 8000 -u -k -e udp-chat-simple.sh

Sprawdzenie działania:

.. code-block:: bash

    exec 5<>/dev/udp/localhost/8000
    echo "TEST" >&5
    cat <&5

Zadanie
-------

Prosty chat.

Proszę uruchomić serwer podany w przykładzie na komputerze z widocznym na wydziale
adresem IP (i odczytywać komunikaty z portu 51234), następinie niech Państwo
zaczną wysyłać broadcasty na ten port.

Mamy bardzo prosty system chatowania (bez serwerów!).

