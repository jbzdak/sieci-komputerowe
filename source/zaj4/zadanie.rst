Zadanie
=======

Stworzenie programu który pozwala na sprawdzenie listy obecności na zajęciach
(widocznej dla każdego użytkownika sieci).

Program po włączeniu wysyła wiadomość broadcast: z informacją "włączył się
użytkownik jacek. Wiadomość ta jest wysyłana co pięć sekund. Program
dodatkowo nasłuchuje takich wiadomości i wyświetla użytkownikowi listę 
wszystkich osób od których otrzymał wiadomość. Na liście znajduje się:

* Imie
* Adres IP
* Port z którego wysłano informację 