UDP w Javie
===========

Unicast/Broadcast
#################


Wszytkie przykłady znajdują się na stronie: `https://github.com/jbzdak/sieci-przyklady/tree/master <https://github.com/jbzdak/sieci-przyklady/tree/master>`_


.. note::

    Java automaycznie wyśle pakiet broadcast jeśli poprosimy ją o wysłanie
    danych na adres broadcast.



Klasa z konfiguracją
--------------------

.. code-block:: java

    import java.net.InetAddress;
    import java.net.UnknownHostException;

    public class Config {
        public static final int PORT = 9000;
        public static final int BUFFER_SIZE = 1024;

        public static final InetAddress MULTICAST_ADDRESS;
        public static final int MULTICAST_PORT = 9000;
        static {
            try{
                MULTICAST_ADDRESS = InetAddress.getByName("239.255.42.99");
            }catch (UnknownHostException e){
                throw new RuntimeException(e);
            }
        }


    }


Server
------


.. code-block:: java

    package pl.edu.pw.fizyka.sk;

    import java.net.DatagramPacket;
    import java.net.DatagramSocket;
    import java.net.InetAddress;

    public class UTPServer {

        public static void main(String[] args) throws Exception{

            //Otwarcie gniazda z okreslonym portem
            DatagramSocket datagramSocket = new DatagramSocket(Config.PORT);

            byte[] byteResponse = "OK".getBytes("utf8");

            while (true){

                DatagramPacket reclievedPacket
                        = new DatagramPacket( new byte[Config.BUFFER_SIZE], Config.BUFFER_SIZE);

                datagramSocket.receive(reclievedPacket);

                int length = reclievedPacket.getLength();
                String message =
                        new String(reclievedPacket.getData(), 0, length, "utf8");

                // Port i host który wysłał nam zapytanie
                InetAddress address = reclievedPacket.getAddress();
                int port = reclievedPacket.getPort();

                System.out.println(message);
                Thread.sleep(1000); //To oczekiwanie nie jest potrzebne dla
                // obsługi gniazda

                DatagramPacket response
                        = new DatagramPacket(
                            byteResponse, byteResponse.length, address, port);

                datagramSocket.send(response);

            }


        }
    }



Ważniejsze miejsca programu
***************************

Przygotowanie do odbierania połączeń UDP na zadanym portcie:

.. code-block:: java

    DatagramSocket datagramSocket = new DatagramSocket(9000);

Stworzenie pakietu który będzie odbierał dane:

.. code-block:: java

   DatagramPacket reclievedPacket
                = new DatagramPacket( new byte[BUFFER_SIZE], BUFFER_SIZE);

Odebraie pakietu:

.. code-block:: java

   datagramSocket.receive(reclievedPacket);

Tutaj jest kolejny detal implementacyjny w Javie: musimy przekształcić 
ciąg bajtów do instancji klasy string. Zakładamy że dane w pakiecie kodowane
są za pomocą utf-9.

.. code-block:: java

    int length = reclievedPacket.getLength();
    String message =
        new String(reclievedPacket.getData(), 0, length, "utf8");

Wysłanie odpowiedzi:

.. code-block:: java

       byte[] byteResponse = "OK".getBytes("utf8");
       DatagramPacket response
                        = new DatagramPacket(
                            byteResponse, byteResponse.length, address, port);

.. note::

    W protokole TCP w serwerze n

Klient
------

Oprogramowanie klienta UDP jest również relatywnie proste.

.. note::

    Nasz klient
    przyjmuje z linii komend dwa argumenty: adres na który wysyła wiadomość oraz
    treść wiadomości.

.. code-block:: java

    package pl.edu.pw.fizyka.sk;

    import java.io.IOException;
    import java.net.DatagramPacket;
    import java.net.DatagramSocket;
    import java.net.InetAddress;
    import java.net.SocketTimeoutException;

    public class UDPClient {

        public static void main(String[] args) throws IOException {

            for(String s: args){
                System.out.println(s);
            }
            //Odczytujemy wiadomosc albo z konsoli albo z pierwszego argumentu
            String message = null;
            if (args.length != 2){
                System.out.println(args.length);
                System.out.println("USAGE: Client <serverAddress> <nessage>");
                return;
            }

            message = args[1];
            InetAddress serverAddress = InetAddress.getByName(args[0]);
            System.out.println(serverAddress);

                    DatagramSocket socket = new DatagramSocket(); //Otwarcie gniazda
            byte[] stringContents = message.getBytes("utf8"); //Pobranie strumienia bajtów z wiadomosci

            DatagramPacket sentPacket = new DatagramPacket(stringContents, stringContents.length);
            sentPacket.setAddress(serverAddress);
            sentPacket.setPort(Config.PORT);
            socket.send(sentPacket);

            DatagramPacket reclievePacket = new DatagramPacket( new byte[Config.BUFFER_SIZE], Config.BUFFER_SIZE);
            socket.setSoTimeout(1010);

            try{
                socket.receive(reclievePacket);
                System.out.println("Serwer otrzymał wiadomość");
            }catch (SocketTimeoutException ste){
                System.out.println("Serwer nie odpowiedział, więc albo dostał wiadomość albo nie...");
            }

        }
    }


Ważniejsze miejsca programu
***************************

Wybieramy do jakiego adresu wysyłamy informacje:

.. code-block:: java

    InetAddress serverAddress = InetAddress.getByName(args[0]);

Wysłanie pakietu:

.. code-block:: java

    byte[] stringContents = message.getBytes("utf8"); //Pobranie strumienia bajtów z wiadomosci

    DatagramPacket sentPacket = new DatagramPacket(stringContents, stringContents.length);
    sentPacket.setAddress(serverAddress);
    sentPacket.setPort(PORT);

Odebranie odpowiedzi.

Tutaj musimy się na chwilę zatrzymać: w ``TCP`` moglibyśmy po prostu
poczekać na odpowiedź od serwera, tutaj nie możemy tak zrobić --- przecież
odpowiedź od serwera może po prostu nie nadejść... należy więc powiedzieć
socketowi: Poczekaj określony czas na odpowiedż, jeśli nie nadejdzie ona
zgłoś wyjątek.

Ustawienie okresu oczekiwania na odpowiedź:

.. note::

    Argument metody ``setSoTimeout`` to maksymalny czas oczekiwania na odpowiedź 
    w milisekundach.

.. code-block:: java

    socket.setSoTimeout(1010);

Odebranie danych:

.. code-block:: java

    DatagramPacket reclievePacket = new DatagramPacket(new byte[Config.BUFFER_SIZE], Config.BUFFER_SIZE);
    try{
        socket.receive(reclievePacket);
        System.out.println("Serwer otrzymał wiadomość");
    }catch (SocketTimeoutException ste){
        System.out.println("Serwer nie odpowiedział, więc albo dostał wiadomość albo nie...");
    }


Multicast
#########

Serwer
------

.. code-block:: java

    package pl.edu.pw.fizyka.sk;

    import java.net.DatagramPacket;
    import java.net.DatagramSocket;
    import java.net.InetAddress;
    import java.net.MulticastSocket;

    public class MulticastServer {
        public static void main(String[] args) throws Exception{

            byte[] responseBytes = "ACK".getBytes();

            InetAddress group = Config.MULTICAST_ADDRESS;
            MulticastSocket s = new MulticastSocket(Config.MULTICAST_PORT);
            s.joinGroup(group);

            try{
                while (true){
                    DatagramPacket recv = new DatagramPacket(new byte[Config.BUFFER_SIZE], Config.BUFFER_SIZE);
                    s.receive(recv);
                    String stringMsg = new String(recv.getData(), 0, recv.getLength(), "utf8");
                    System.err.println("Got message: \"" + stringMsg + "\"");
                    DatagramSocket responseSocket = new DatagramSocket();
                    DatagramPacket response = new DatagramPacket(responseBytes, responseBytes.length);
                    response.setAddress(recv.getAddress());
                    response.setPort(recv.getPort());
                    Thread.sleep(1000); // Ta linijka powoduje wstrzymanie wysyłania odpowiedzi przez
                    // jedną sekundę --- nie ma ona związku z obsługą UDP.
                    responseSocket.send(response);
                }
            }finally {
                s.leaveGroup(group);
            }
        }
    }



Ważniejsze miejsca programu
***************************

Stworzenie gniazda multicast --- oraz dołączenie do grupy mulitcast

.. code-block:: java

    InetAddress group = Config.MULTICAST_ADDRESS;
    MulticastSocket s = new MulticastSocket(Config.MULTICAST_PORT);
    s.joinGroup(group);

Klient
------

.. code-block:: java

    package pl.edu.pw.fizyka.sk;

    import java.net.DatagramPacket;
    import java.net.DatagramSocket;
    import java.net.SocketTimeoutException;

    public class MulticastClient {

    public static void main(String[] args) throws Exception{
            DatagramSocket s = new DatagramSocket();

            byte[] message = "Test".getBytes("utf8");


            DatagramPacket packet = new DatagramPacket(message, message.length);
            packet.setPort(Config.PORT);
            packet.setAddress(Config.MULTICAST_ADDRESS);
            s.send(packet);

            System.out.println("Wysłałem pakiet");
            s.setSoTimeout(1000);
            DatagramPacket response = new DatagramPacket(new byte[Config.BUFFER_SIZE], Config.BUFFER_SIZE);
            try{
                s.receive(response);
                System.out.println("Odpowiedź: ");
                System.out.print(new String(response.getData(), 0,  response.getLength(), "utf8"));
            }catch (SocketTimeoutException e){
                System.out.println("Nie otrzymałem odpowiedzi");
            }

        }
    }



Ważniejsze miejsca programu
***************************

Wysłanie pakietu na grupę multicast:

.. code-block:: java

    DatagramPacket packet = new DatagramPacket(message, message.length);
    packet.setPort(Config.PORT);
    packet.setAddress(Config.MULTICAST_ADDRESS);
    s.send(packet);

Ustawienie timeoutu na odowiedź:

.. code-block:: java

    s.setSoTimeout(1000);