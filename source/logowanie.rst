Logowanie na konta na serwerze ``student``
==========================================

Logowanie na konta na studencie za pomocą Gnome3 jest proste.

Najpierw należy uruchomić eksplorator plików: na przykład
w konsoli wpisać ``nautilus``, albo kliknąć na ikonę na bocznynm panelu:

.. figure:: logowanie/nautilus.png

    Otwarcie nautilusa


Następnie uruchamiamy menu nautilusa i wybieramy: ``Connect to Server``:

.. figure::  logowanie/nautilus.png

    Menu Connect to server

Następnie wpisujemy adres: ``ssh://student.if.pw.edu.pl``.


.. figure::  logowanie/connect-to-server.png

    Wpisanie adresu.


Potem użytkownik i hasło. Katalog jest otwarty

