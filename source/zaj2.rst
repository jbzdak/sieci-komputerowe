Zajęcia 2: Skrypty w Bash
=========================


.. contents:: Spis treści

Dlaczego uczymy się Bash
------------------------

To prawda, że Bash jest narzędziem dość starym (co widać przy jego używaniu),
ma nowsze odpowiedniki, które nie mają jego wad.

Jednak:

1. Bash jest zainstalalowany w (prawie) każdej dystrybucji Linuksa.
   Więc jeśli chcecie napisać przenośny skrypt, to bash jest rozwiązaniem.
   W systemach, w których nie ma powłoki ``bash`` **musi** być zainstalowana
   powłoka ``sh``, która jest podzbiorem bash.
2. Prawie każde oprogramowanie na Linuksie korzysta ze skryptów napisanych w
   Bash, więc trzeba umieć przynajmniej czytać skrypty bash.


Zmienne w Bash: 10min
---------------------

Bash nie jest tylko konsolą, posiada zmienne.

.. code-block:: bash

    jb@gautama:/$ foo="bar";
    jb@gautama:/$ echo $foo;
    bar
    jb@gautama:/$ echo ${foo}; #Równoważne do $foo
    bar

Uwaga: Białe znaki mają znaczenie: tj. poniższy
kod jest niepoprawny: ``foo = "bar"``


Polecenie `export`
^^^^^^^^^^^^^^^^^^

Export powoduje, że zmienne są dostępne dla podprocesów.

.. code-block:: bash

    export foo="bar";

Nie ma możliwości udostępnienia zmiennych dla nadprocesów.


Ustawianie zmiennych środowiskowcyh
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By ustawić zmienne środowiskowe 'na stałe'
należy umieścić odpowiednie polecenia
``export`` w plikach:


``/etc/bash.bashrc``
    wtedy zmienne będą widoczne dla wszystkich użytkowników

``~/.bashrc``
    wtedy będą widoczne dla danego użytkownika.


Zmienna path
------------

Zmienna path jest magiczną zmienną powłoki ``BASH``, pozwala ona na zdefiniowanie
w jakich katalogach ``bash`` szuka plików wykonywalnych.

.. note::


    Zmienna ``$PATH`` określa listę katalogów, które będą
    przeszukiwane podczas uruchamiania programu.

    Jeśli ``$PATH`` to ``/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games``
    oznacza to, że po wykonaniu polecenia np. ``nano`` zostaną
    przeszukane takie położenia: ``/usr/local/bin/nano``,
    ``/usr/bin/nano``. itp.

Wszystkie zmienne środowiskowe można wyświetlić za pomocą polecenia ``printenv``.


Ćwiczenie 1
^^^^^^^^^^^

Proszę ustawić zmienną ``PATH`` na pusty ciąg znaków i sprawdzić jakie polecenia działają.

Czy działa:

* ``cd``
* ``pwd``
* ``ls``
* ``cat``
* ``grep``

Polecenia, które działają są **wbudowanymi** poleceniami powłoki bash, pozstałe
są programami.

Cytowanie
---------

Powiedzmy, że mamy katalog o nazwie ``Moje Dokumenty``, który znajduje się 
w bieżącym katalogu. Chcemy go usunąć, jednak nie udaje się to:

.. code-block:: bash

    rm Moje Dokumenty
    rm: cannot remove ‘Moje’: No such file or directory
    rm: cannot remove ‘Dokumenty’: No such file or directory

Okazuje się, że bash potraktował ``Moje Dokumenty`` jako dwa katalogi,
jeden o nazwie ``Moje``, drugi o nazwie ``Dokumenty``.

By usunąć ten plik należy użyć cytowania, czyli powiedzieć bashowi:
ten ciąg znaków ze spacją jest jednym słowem, służą do tego podwójne cudzysłowy.


Taka komenda zadziała::

    rm "Moje Dokumenty"

Powiedzmy, że nasz skrypt chce wyświetlić użytkownikowi napis:
``Dodaj katalog foo do zmiennej ${PATH}``.

Polecenie::

    echo "Dodaj katalog foo do zmiennej ${PATH}"

wyświetli::

    Dodaj katalog foo do zmiennej /usr/local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

Znów musimy zacytować nasz ciąg znaków, ale tym razem trzeba użyć pojedyńczego
apostrofu::

    echo 'Dodaj katalog foo do zmiennej ${PATH}'

Wyświetla::

    Dodaj katalog foo do zmiennej ${PATH}


Linia hashbang
--------------

Skrypty ``bash`` mogą być uruchamiane tak samo jak zwykłe programy, o ile:

* nadano im uprawnienia do wykonywania ``chmod +x nazwa.sh``
* rozpoczynają się od linii hashbang, czyli od: ``#!/bin/bash``, w linii tej
  po znakach ``#!`` znajduje się pełna ścieżka do interpretera, który wykona
  ów program.

Przykład najprostszego skryptu:

.. code-block:: bash

    #!/bin/bash
    echo hello ze skryptu


Ćwiczenie 2
-----------

Napisać skrypt, który wypisze 3 linijki:

#. Ścieżka do Twojego katalogu domowego (np. /home/foo)
   (skorzystać z odpowiedniej zmiennej środowiskowej)
#. ${HOME} (w konsoli ma się pojawić dokładnie taki napis)
#. aktualna data to: pon, 9 mar 2015, 17:47:38 CET (skorzystać z polecenia date)

Zapisywanie wyniku polecenia w zmiennej
---------------------------------------

Czasem chcemy zapisać wynik polecenia do zmiennej, np. polecenie date wyświetla
do wyświetlania daty służy polecenie date::

    date +%Y-%m-%d

wyświetla::

    2015-10-05


Chcemy stworzyć plik o nazwie ``kopia-zapasowa-data``::

    curr_date=$(date +%Y-%m-%d)
    file_name="kopia-zapasowa-${curr_date}"
    echo > $file_name

można to zrobić prościej::

    touch "kopia-zapasowa-$(date +%Y-%m-%d)"

.. note::

    **Uwaga!** Działanie ``$()`` można osiągnść również poprzez zastosowanie
    odwróconych cudzysłowów (`), tj::

        touch "kopia-zapasowa-`date +%Y-%m-%d`"

    Jednak by oszczędzić Wam problemów z `, ' oraz ", będę wykorzystywał ``$()``.

Specjalne zmienne
-----------------

Skrypt basha zawiera specjalne zmienne:

* ``$0`` nazwa skryptu
* ``$1-$9`` argumenty skryptu
* ``$#`` liczba argumentów
* ``$@`` wszystkie argumenty na raz

Ćwiczenie 3
-----------

Napisać skrypt (i uruchomić z 3. dowolnymi argumentami), który:

a. wypisze liczbę argumentów skryptu
b. wypisze drugi argument
c. wypisze wszystkie dostępne zmienne środowiskowe, które w swojej wartości
   zawierają Państwa nazwę użytkownika (użyć zmiennej $USER)

Instrukcje warunkowe
--------------------

Kod wyjścia z programu
^^^^^^^^^^^^^^^^^^^^^^

Wykonanie każdego programu może skończyć się albo
sukcesem, albo porażką, do syngalizowania sukcesu lub porażki służy
tzw. kod wyjścia.

.. note::

    W ``C`` kod wyjścia programu jest sygnalizowani (między innymi) wartością 
    zwracaną przez funkcję ``main``.


Jeśli program wykonmał się poprawnie funkcja zwraca ``0``, jeśli nie poprawnie
inną wartość.

.. note::

    Jest to notacja odwrotna od tej stosowanej w ``C``, tam ``0`` oznacza fałsz,
    a ``1`` prawdę.

    Powód takiej notacji w jest prosty: Program może poprawnie się wykoanć tylko
    w jeden sposób, może natomiast napotkać na wiele problemów, które spowodują 
    błędne wykonanie.

Do sprawdzenia kodu wyjścia ostatniego polecenia można użyć magicznej zmiennej
``$?``.

Instrukcje warunkowe
^^^^^^^^^^^^^^^^^^^^

Instrukcja ``if`` w Bash ma następującą składnię::

    if warunek
    then

    fi

Gdzie ``warunek`` jest **dowolnym programem**. Jeśli program ten zwróci kod
wyjścia  ``0`` warunek się wykona, nie wykona się natomiast jeśli zwróci inny
kod wyjścia.

Przykładowo by sprawdzić czy ``foo`` jest nazwą użytkownika można użyć::

    if grep foo /etc/passwd > /dev/null
    then
     echo "Jest"
    fi

.. note::

    Uwaga zastosowałem tu nową składnię polecenia ``grep`` jest to składnia
    ``grep wzorzec nazwa pliku`` oraz przekierowałem wyjście z ``grep`` do
    pliku ``/dev/null``.


Else, elsif
^^^^^^^^^^^

Pełna składnia ``if`` jest taka::

    if warunek1
    then
        polecenie1
    elif warunek2
    then
        polecenie2
    else
        polecenie3
    fi

Polecenie test
^^^^^^^^^^^^^^

W warunku ``if`` często korzysta się z polecenia ``test``, na przykład
``test -e plik`` sprawdza czy plik istnieje::

    if test -e /tmp/foo
    then
        echo istnieje
    else
        echo nie istnieje
    fi

Polecam sprawdzenie jak działa polecenie test za pomocą ``man test``.

Skrócona wersja polecenia test
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Polecenie ``test`` można wykorzystać również za pomocą krótszej postacji, tj.
nawiasów kwadratowych::


    if [ -e /tmp/foo ]
    then
        echo istnieje
    else
        echo nie istnieje
    fi

Uwaga: między ``[`` oraz ``]``, a warunkiem musi znajdować się przynajmniej
jedna spacja.



Ćwiczenie 4
-----------

Napisać skrypt, który:

a. sprawdzi czy istnieje plik  ``~/.xsession-errors`` i wypisze odpowiednią informację
b. porówna liczbę plików w katalogu głównym oraz domowym i wypisze odpowiednią informację

   .. note::

      Polecam program ``wc`` (proszę przeczytać ``man wc``)

Obliczenia arytmetyczne
-----------------------

Do obliczeń arytmetycznych służy funkcja ``let``::

    let x=10
    echo $x # wypisze 10
    let "x /= 3"
    echo $x # wypisze 3
    let "x *= x"
    echo $x # wypisze 9

.. warning::

    Bash może wspierać tylko arytmetykę stałoprzecinkową, dla której ``10/3``
    wynosi dokładnie ``3``.

.. warning::

    Przy wykonywaniu operacji należy umieszczać spacje wokół operatora ``=``.

Funkcje
-------

Niektóre fragmenty skryptu można zamknąć w funkcję, składnia funkcji jest
następująca::

    function func {
      # Treść
    }

By wykonać funkcję należy napisać jej nazwę.

Do funkcji można przekazywać parametry. Parametry funkcji są dostępne za pomocą
tych samych zmiennych co argumenty skyptu, tj:

* ``$1-$9`` argumenty funkcji
* ``$#`` liczba argumentów
* ``$@`` wszystkie argumenty na raz


Pętle
-----

Pętla ``for`` pozwala na iterowanie po zbiorze elementów separowanych spacją::

    for i in a b c d
    do
        echo "wartość iteratora to $i"
    done

    for i in /etc/*conf
    do
        echo "$i"
    done

.. note::

    Można skonfigurować basha tak, by separatorem był inny znak, ale poprzestańmy
    na spacji.

``while`` - sprawdza, czy warunek jest prawdziwy, jeśli tak,
pętla wykonuje się, aż warunek stanie się fałszywy::

    a=0
    while [ $a -lt 5 ]
    do
        echo "pętla wykonuje się po raz: $a"
        a=$[a + 1] # lub a=$(( $a + 1 ))
    done

``until`` - sprawdza, czy warunek jest fałszywy, jeśli tak, pętla wykonuje się aż warunek stanie
się prawdziwy::

    a=0
    until [ $a -gt 5 ]
    do
        echo "pętla wykonuje się po raz: $a"
        a=$[a + 1]
    done

Ćwiczenie 5:
------------

Napisz funkcję, która liczy silnię jej argumentu, wykonaj ją dla liczb od 1 do 15.


Wyrażenia regularne
-------------------

Wyrażenia regularne są narzędziem pozwalającym na:

* Definiowanie zbiorów ciągów znaków
* Sprawdzanie czy dany ciąg znaków należy do takiego zbioru
* Przeszukiwania ciągu znaków, w poszukiwaniu podciągów będących
  elementem takiego zbioru

Wprowadzenie do wyrażeń regularnych
***********************************

Wyrażenia regularne są narzędziem pozwalają na definiowanie zbiorów ciągów
znaków.

* Wyrażenie regularne: ``a`` definuje zbiór ciągów znaków zawierający ciąg ``a``
* Wyrażenie regularne: ``Romeo`` definuje zbiór ciągów znaków zawierający ciąg ``Romeo``

Znaki ``*`` oraz ``+`` powodują że poprzedni ciąg jest powtarczny, ``+`` oznacza
jedno lub więcej powtórzenie, ``*`` zero lub więcej:

* Wyrażenie regularne ``a*`` definiuje zbiór ciągów znaków ``∅, a, aa, aaa, aaaa, aaaaa, ...``
* Wyrażenie regularne ``a+`` definiuje zbiór ciągów znaków ``a, aa, aaa, aaaa, aaaaa, ...``
* Wyrażenie regularne ``Romeo*`` definiuje zbiór ciągów znaków ``Rome, Romeo, Romeoo, Romeooo, ...``

Nawiasy ``()`` służą do grupowania ciągów znaków:

* Wyrażenie regularne ``(Romeo)*`` definiuje zbiór ciągów znaków ``∅, Romeo, RomeoRomeo, ...``

Nawiasy ``[]`` służą do definiowania grup znaków:

* Wyrażenie regularne ``[abc]`` definiuje zbiór ciągów znaków ``a, b, c``
* Wyrażenie regularne ``[abc]+`` definiuje zbiór ciągów znaków ``a, b, c, aa, ab, ac, ...``

Predefiniowane zestawy znaków:

* Wyrażenie: ``\w`` oznacza dowolny znak tworzący wyraz (często, bez polskich znaków)
* Wyrażenie: ``\d`` oznacza dowolną cyfrę 
* Wyrażenie: ``.`` oznacza dowolny znak

By wybrać znak który normalnie pełni specjalną rolę w wyrażeniach regularnych
(np. * lub nawias) należy postawić przed nim ukośnik ``\``.

Znak ``|`` oznacza operator lub:

* Wyrażenie ``(Romeo)|(Juliet)`` definiuje zbiór ciągów znakow: ``Romeo, Juliet``

Polecenie ``sed`` jest bardzo potężnym "strumieniowym edytorem" tekstu, który
polega na wyrażeniach regularnych, przykład::

    echo 'samogłoski stają się kropkami' | sed 's/[aeouiy]/./g'

Sed przyjmuje ciąg znaków (jest on tutaj cytowany, ponieważ zawiera również specjalne
znaki powłoki bash), ciąg ten ma taki format ``s/wyrażenie reg/zastępnik/g``,
gdzie ``wyrażenie reg`` jest wyrażeniem regularnym, którego wystąpienia
zostaną zamienione na ``zastępnik``.

Wyrażenia regularne obsługują również grupy znaków które są definiowane za pomocą 
zwykłych nawiasów. Przy zastępowaniu przez ``sed`` (i inne programy) w
zastępniku mogą pojawiać się ciągi znaków
w formacie ``\1``, ``\2``, ``\3``, które zostaną zastąpione przez to co zostanie
wybrane przez grupę o odpowiednim numerze::


   echo 'zmiana . na , w liczbach rzeczywistych: 3.14' | sed 's/\([0-9]\+\)\.\([0-9]\+\)/\1,\2/g'

Wyrażenie to zawiera dwie grupy oddzielone kropką, pierwsza z nich wybiera
ciąg znaków ``3``, druga ``14``, następnie do zastępnika w formacie ``\1,\2``
podstawia te dwie grupy, co daje ``3,14``.


Zadanie:
--------

Proszę pobrać tekst Romeo i Julii po angielsku::

    wget http://db.fizyka.pw.edu.pl/sk-2015/_downloads/pg1112.txt


1. Za pomocą edytora sed zamienić wszystkie miejsca gdzie znajduje się ciąg
   ``Juliet`` na ``Romeo`` (i na odwrót).


Instalacja powłoki bash na Windowsie
------------------------------------

Do instalacji powłoki Bash na Winowsie można wykorzystać 
`projekt Cygwin: <https://www.cygwin.com/>`__, jest to projekt udostępniający środowisko
POSIX-owe (POSIX to standard definiujący Linuxa) na systemach MS Windows.


Praca domowa
------------

Praca domowa :doc:`zaj2/prace`.