Praca domowa 1: Skypty bash
===========================

.. warning::

    Uwaga ta informacja dotyczy grup piątkowych.


Treści zadań dostępne są na stronie: ``http://www.if.pw.edu.pl/~maszyman/dydaktyka/SK14L/projekty/sk_skrypt_zadanieXXX.pdf``
gdzie XXX przyjuje wartości od ``1`` do ``10``.

Każda osoba powinna wybrać sobie zadanie jakie najbardziej jej pasuje.

Zadania oczywiście należy przesłać na inny adres ``bzdak@*`` oraz w terminie
określonym na zajęciach.