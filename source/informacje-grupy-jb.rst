(!! Wyniki kolokwiów !!)  Informacje dla grup prowadzonych przez J. Bzdaka
==========================================================================

.. note::

  Są to grupy:

  * Piątek 11-14
  * Poniedziałek 8-11 (nieparzyste)

Termin konsultacji
------------------

Wyznaczam konsultacje w następujących terminach:

* Wtorki 12-12.45 sala 231
* Piątki 14-14.45 sala 231


Poprawy kolokwiów/projekty
--------------------------

.. note::

    Pojawiły się nowe wyniki popraw. Osoby ciągłe zainteresowane poprawami
    zapraszam do zgłoszeń w przyszły wtorek.

* Wyniki kolokwiów (wszystkich grup) są do pobrania tutaj:
  :download:`wyniki/oceny.csv`. Wyniki są dostępne po numerze indeksu,
  osoby które się nie mogą znaleźć proszę o kontakt (możliwe że źle spisałem
  numer z kolokwium).
* Projekty można oddać do sesji, zapraszam na konsultacje oraz po piątkowych
  zajęciach. Osoby którym terminy te nie pasują proszę o kontakt.
* Możecie również poprawić kolokwia z części laboratoryjnej --- tutaj zapraszam
  **na ostatnie zajęcia**. Przed ew. poprawą proszę o kontakt mejlowy.
* Zapraszam również do poprawy kolokwiów z wykładu. Przy czym: poprawić można tylko
  wszystkie kolokwia wykładowe na raz.
