Instrukcja dla prowadzącego zajęcia (zaj 1)
===========================================

1. Należy zgłosić się do mnie lub dr. Bajceckiego celem stworzenia konta
   z uprawinieniem ``sudo`` na maszynach w sali 231.
2. Całe zajęcia przeprowadzamy za **na maszynach wirtualnych**
3. Jeśli ktoś skopie instajacje może importować "gotowy" obraz klikając
   ``File -> Import Appliance`` i wybierając ``/opt/sk/share/vm/*ova``.
4. Jeśli grupie idzie dobrze to robicie całość, jeśli gorzej można pominąć:
   "Sygnały" oraz "Specjalne pliki 5min". Dobrze jest zawsze zrobić
   "Zadanie Końcowe".

Rozwiązania
-----------

Pisane z pamięci

1. .. code-block:: bash

      cd /etc
      echo *conf

2. .. code-block:: bash

       wget http://db.fizyka.pw.edu.pl/sk-2015/_downloads/pg1112.txt
       pwd
       cd /tmp
       mkdir foo bar
       cd foo
       cp ~/pg1112.txt .
       cd ../bar
       cat ../foo/pg*

3. .. code-block:: bash

      wget http://www.gutenberg.org/cache/epub/1112/pg1112.txt

      cat pg1112.txt | grep Juliet
      cat pg1112.txt | grep -n Juliet
      cat pg1112.txt | grep -n Juliet | grep Romeo

4. .. code-block:: bash

      cat /etc/passwd | cut -d : -f 1

5. .. code-block:: bash

      tar -c /tmp/foo | bzip2 -c > /tmp/foo.tar.bz2






