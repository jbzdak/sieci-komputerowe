Instrukcja dla prowadzącego zajęcia (zaj 5)
===========================================


Zad B
-----

::

  apt-cache search -n ssh | grep server

Są dwie paczki które instalują (ten sam server ssh):

* ``task-ssh-server``
* ``openssh-server ``

Instalacja::

  apt-get update
  apt-get install openssh-server

Zad C
-----

Znaleźć linie (ew. dodać) o treści::

  PermitRootLogin no
  DenyUsers foo

do pliku ``/etc/sshd_config``

Zad D
-----

Możecie przypomnieć studentom o ``scp``.


Zad E
-----

::

  ufw default deny
  ufw enable # SSH nie działa
  ufw allow 22 # działa
  ufw reset # wyłączamy
  ufw disable # wyłączamy

Zad F
-----

.. note::

  Najpierw dodajemy kolejny interfejs sieciowy do maszyny wirtualnej, musi być to
  ``Bridged Adapter``, przypięty do ``eth0`` na maszynie matce. Będzie on widoczny
  jako ``eth2`` na dziecku,  by pobrać adres za pomocą DHCP na dziecku należy
  napisać::

    dhclient eth2

  Oczywiście normalnie powinno się np. edytować ``/etc/network/interfaces``, albo
  zmienić w ``network-manager``, ale tak jest prościej (a network manager słabo
  radzi sobie z trzema takimi samymi kartami sieciowymi)

By udostępnić:

* Dodać do etc/exports::

  /home/student/foo 192.168.1.0/24(rw,sync)

* By używać::

    mkdir /tmp/foo
    mount 192.168.1.XX:/home/student/foo /tmp/foo

