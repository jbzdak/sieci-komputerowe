
System plików w Linuksie
------------------------

Głównym węzłem jest abstrakcyjny katalog ``/``, a dyski mogą 
być podpięte zarówno w węźle ``/`` jak i w dowolnym innym miejscu.

Zawartość ``/`` jest wyspecyfikowana w standardzie ``Posix``

``/home``

    Zawiera katalogi domowe użytkowników

``/bin``

    Zawiera pliki wykonywalne.

``/boot``

    Zawiera pliki rozruchowe (jądro systemu)

``/dev``

    Zawiera pliki urządzeń.

    .. note::

        Jedną z zasad *filozofii UNIXA* jest **Wszystko jest plikiem**,
        co oznacza, że np. napęd cd jest w systemie reprezentowany
        jako specjalny plik ``/dev/cdrom``.

``/etc``

    Pliki konfiguracyjne

``/tmp``

    Katalog na pliki tymczasowe

``/usr``

    Hierarchia plików drugiego poziomu. Znajdują się tutaj rzadziej
    używane pliki.

    ``/usr/bin``

        Zawiera rzadziej używane pliki wykonywalne

    ``/usr/lib``

        Proszę zgadywać...

    .. note::

        Taka budowa systemy UNIX wynikała z faktu, że dawno temu dyski
        komputerów były wolne, małe i drogie.

        W ``/bin`` i ``/lib`` trzymano tylko pliki potrzebne do działania
        systemu, a różne narzędzia umieszczano w ``/usr/..``, który
        albo był ustawiony na większym i wolniejszym dysku, albo
        nawet udostępniany z serwera przez NFS.

    ``/usr/bin/local``

        Hierarchia trzeciego rzędu :) Tak: ``/usr/local/bin`` też jest :).

``/proc``

    Wirtualny system plików zawierający informacje o procesach
    uruchomionych w systemie oraz inne ciekawe dane.

    ``/proc/cpuinfo``

        Informacje o procesorze

    ``/proc/self/status``

        Dane o aktualnym procesie.
