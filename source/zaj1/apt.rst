Manager paczek w systemie Debian Linux
--------------------------------------

W Linuksach zasadniczo całe oprogramowanie, które użytkownik
chciałby zainstalować jest dostarczane przez twórców danej
dystrybucji. Wszystkie programy są przechowywane w
**repozytorium**, skąd można je pobierać.

Pobieranie  i instalacja programów z repozytorium ma takie przewagi nad
pobieraniem ich z sieci:

* **Jest wygodniejsze** Wystarczy wpisać jedno polecenie.
* **Zwiększa bezpieczeństwo**: Zdarza się, że programy dostępne w
  Internecie są zainfekowane. Wersja do pobrania z repozytoriuów
  jest przejrzana przez przynajmniej jedną osobę, która sprawdza
  bezpieczeństwo tego oprogramowania.

  Jeśli program jest pobierany przez ``HTTP`` czy ``FTP`` może
  zostać zainfekowany po drodze (o tym dlaczego tak jest na
  wykładzie). Program ``apt`` zarządzający pakietami Debiana
  może wykryć takie ataki.

System debian używa managera paczek ``dpkg``, wraz z programami
do zarządzania paczkami ``apt`` i ``aptitude``.

Żródła, z których można pobierać oprogramowanie zdefiniowane
są w pliku: ``/etc/apt/sources.list`` i zasadniczo wyglądają tak::

    deb http://ftp.de.debian.org/debian stable main contrib non-free

gdzie:

``http://ftp.de.debian.org/debian`` to główny katalog mirrora.

``stable`` to dystrybucja z której pobieramy dane

``main contrib non-free`` sekcje z których pobieramy dane

**Dystrybucje sysemu debian**

System debian wydaje kilka dystrybucji, tj. kilka zestawów
głównych programów. Są to:

``stable``

    Bardzo stabilna. Wydawana raz na dwa lata. Nie pojawia
    się w niej nowe oprogramowanie, dodawane są tylko poprawki
    bezpieczeństwa.

``testing``

    Dość stabilna. Ciągle pojawia się nowe oprogramowanie.
    Mogą się pojawić błędy, ale raczej nie będą katastrofalne.

``unstable``

    Niestabilna. Mogą pojawić się błędy np. uniemożliwiające
    załadowanie systemu poeracyjnego

``experimental``

    Jak ``unstalbe`` tylko bardziej.

**Sekcje**

Każda dystrybucja podzielona jest na sekcje

``main``

    Zawiera oprogramowanie przetestowane najlepiej.

``contrib``

    Zawiera rzadziej używane oprogramowanie.

``non-free``

    Zawiera oprogramowanie, które nie jest udostępniane
    na Wolnej licencji.
