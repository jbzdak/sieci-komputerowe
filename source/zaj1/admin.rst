
Podstawy administracji systemem linux
-------------------------------------

Użytkownikiem administracyjnym jest użytkownik ``root``, automatycznie
ma on każde uprawnienie w systemie. My tak skonfigurrowaliśmy nasz system,
by nie można było logować się na to konto.

Do chwilowego uzyskania uprawnień root służy polecenie ``sudo``.

``sudo <polecenie>``

    wykonuje ``<polecenie>`` jako użytkownik root

``sudo -i``

    Daje dostęp do konsoli roota.
