Zajęcia 6
=========

Dodawanie i usuwanie użytkowników
---------------------------------

Do dodawania użytkownikow służy komenda ``adduser``, a do ich kasowania
``deluser``.

Przy dodawaniu użytkowników można:

* Ustawić domyślną powłokę dla użytkownika (np. na ``/bin/sh``, albo ``/bin/false``),
* Ustawić położenie katalogu domowego,
* Wyłączyć możliwość logowania hasłem,
* Wyłączyć możliwość logowania w ogóle.

.. note::

  Każda usługa działająca w systemie Linux musi działać jako jakiś użytkownik,
  najczęściej **nie powinien to być użytkownik ``root``**. Dobrym zwyczajem jest
  stworzenie oddzielnego użytkownika na każdą z usług, które uruchamiacie na
  swoim systemie.

Wszystkie informacje o użytkowniku są w ``/etc/passwd``, skrót hasła jest w pliku
``/etc/shadow``.

Grupy
-----

Użytkownik może należy do jednej bądź wielu grup. Grupy ułatwiają kontrolę 
wielu użytkowników.

By dodać użytkownika do grupy można wykonać::

  adduser username group

Zarządzanie uprawnieniami ``sudo``
----------------------------------

Sudo pozwala na trochę lepszą kontrolę, niż tylko nadanie uprawnień do
wykonania dowolnej komendy.

Pozwala na:

* Umożliwienie użytkownikowi wykonania tylko kilku poleceń z uprawnieniami root.
* Umożliwienie wykonania niektórych poleceń bez podania hasła.

Konfiguracja polecenia sudo jest w pliku ``/etc/sudoers``, jednak plik ten nie
może być po prostu edytowany --- jeśli jego składnia będzie niepoprawna, to
kolejne polecenia ``sudo`` nie będą mogły się wykonać.

Do edycji pliku ``/etc/sudoers`` służy polecenie ``visudo``.


.. note::

  Nie jest łatwo **bezpiecznie** skonfigurować sudo, przykładowo jeśli dacie
  komuś dostęp do wykonywania edytora ``vim`` z uprawnieniami ``root``
  to, zasadniczo, może on wykonywać dowolne operacje na systemie (ponieważ edytor
  ``vim`` pozwala na uruchomienie dowolnej komendy.


Zadanie A
*********

Domyślna konfiguracja sudo, pozwala na wykonanie polecenia z uprawnieniami
administratora każdemu użytkownikowi w grupie ``sudo``.

Dodaj użytkownika test i nadaj mu uprawnienia do wykonywania dowolnych poleceń.

Zadanie B
*********

Dodaj użytkownika ``user``, który za pomocą polecenia ``sudo`` może tylko
zrestartować komputer (wykonać ``/sbin/reboot``).


Plik ``/etc/hosts``
-------------------

Zanim powstał system DNS, użytkownicy Internetu trzymali odzworowanie
z domen internetowych na IP w pliku ``hosts``. Jest to zwykły tekstowy plik,
na linuksach znjdujący się w ``/etc/hosts``.

.. note::

  Jeśli domena jest wpisana ``/etc/hosts`` zawsze będize używany IP z tego pliku,
  a komputer w ogóle nie wykona zaputania DNS.

Pliki hosts są używane do dziś, np. w małych firmach, które nie chcą stawiać 
firmowych serwerów DNS.

Protokół HTTP
-------------

Protokół HTTP jest protokołem tekstowo-binarnym. Zapytanie HTTP ma następującą
strukturę::

  GET / HTTP/1.1
  Host: localhost

GET jest tzw. metodą, poza ``GET`` jest kilka innych metod. Metoda GET służy
do pobierania zasobów z serwera.

Po metodzie GET następuje ścieżka, która obsłuży zapytanie, w naszym przypadku
jest to: ``/``. ``HTTP/1.1`` informuje o wspieranej wersji protokołu.

Potem następuje lista tzw. nagłówków, po jednym nagłówku w każdej linii, potem
może następować "ciało zapytania", którego u nas nie ma.

Nagłówek ``Host`` mówi serwerowi: "Hej chce pobrać zawartość ze strony localhost".

Server odpowiada::

  HTTP/1.1 403 Forbidden
  Server: nginx/1.6.2
  Date: Sat, 12 Dec 2015 19:11:04 GMT
  Content-Type: text/html
  Content-Length: 168
  Connection: keep-alive
  <html>
  <head><title>403 Forbidden</title></head>
  <body bgcolor="white">
  <center><h1>403 Forbidden</h1></center>
  <hr><center>nginx/1.6.2</center>
  </body>
  </html>

Pierwszą linikją odpowiedzi jest: ``HTTP/1.1 403 Forbidden``. Numer 403 to tzw.
status odpowiedzi, w tym przypadku nie miałem uprawnień do pobrania zasobu.

Następnie lista nagłowków (po jendym w linii) kończąca się pustą linią.

Nagłówki mówią nam o:

* Tym, że zawartość jest typu ``text/html``.
* Że zawartości będzie 168 bajtów.

Serwer następnie odsyła dokładnie 168 bajtów odpowiedzi.

.. note::

  Jest już HTTP w wersji 2.0, ale jest on znacznie trudniejszy w obsłudze, bo
  jest całkiem binarny.

Instalacja serwera nginx
------------------------

Server nginx jest typowym serverem WWW, który potrafi:

* Bardzo szybko wysyłać statyczne pliki.
* Bardzo szybko przesyłać żądania do programów dokonujących dynamicznego ich
  przetwarzania.

.. note::

  Pokazuje Wam server ``nginx``, który obecnie na rynku jest równie popularny co
  ``apache2``. To, który z serverów wykorzystać w konkretnym przypadku wymaga
  zawsze dokładnej oceny.

  Na zajęciach ``nginx`` ma przewagę ponieważ jego konfiguracja jest odrobinę
  prostsza.


Zadanie C
*********

Zainstalować server nginx oraz paczkę ``openjdk-7-doc``.

Zadanie D: Prosta konfiguracja serwera
**************************************

Po instalacji server nginx odpowiada na zapytania
``HTTP`` (wejdźcie na adres ``localhost``).

Konfiguracja servera ``nginx`` znajduje się w katalogu ``/etc/nginx``,
a konfiguracja domyślnej strony w ``/etc/nginx/sites-available/default``,
domyślnie ``nginx`` serwuje katalog ``/var/www/html``.

Serwer jest konfigurowany za pomocą serii bloków ``server``::


  server {
    listen   80 default_server; ## listen for ipv4; this line is default and implied
    listen   [::]:80 default_server ipv6only=on; ## listen for ipv6

    root /var/www/html;
    index index.html index.htm;

    server_name localhost;

    location / {
      try_files $uri $uri/ /index.html;
    }
  }

Tak skonfigurowany serwer będzie odpowiadać na zaptania przychodzące na port
80, a nagłówkiem ``Host`` równym localhost. O tym na jakie domeny reaguje dana
sekcja serwer jest określane w dyrektywie: ``server_name``.

Przeglądarka po wejściu na adres: ``localhost/foo.html``, wykona zapytanie o treści::

  GET /foo.html HTTP/1.1
  Host: localhost
  ...

Serwer zwróci zawartość pliku o nazwie ``foo.html`` w hatalogu ``/var/www/html``.
Katalog ten określony jest w dyrektywie ``root`` (``root`` oznacza katalog głowny
serwera a nie użytkownika ``root``).

Jeśli przyjdzie zapytanie na ścieżkę ``/``::

    GET / HTTP/1.1
    Host: localhost

Serwer zwróci plik ``index.html``.

Konfiguracja w tym wypadku określa, że będzie obsługiwała nie tylko zapytania
na host ``localhost``, ale również na wszystkie inne hosty, mówi o tym
opcja ``default_server`` w dyrektywach ``listen``.

Proszę:

1. Dodać plik o nazwie ``foo.html`` i sprawdzić że wyświetla on się w przeglądarce.

Zadanie E
*********

Protokół HTTP umożliwia serwowanie wielu "stron WWW", albo inaczej, wielu domen
z jednego komputera.

Spróbujemy tak skonfigurować nasz serwer, by przy zapytaniu o domenę ``onet.pl``,
listował zawartość katalogu ``/usr/share/doc`` (katalog ten zawiera
dokumentację różnych paczek systemowych --- oddzielną od systemu ``man``).

.. note::

  Celowo wybrałem **dobrze znaną** domenę, tj. ``onet.pl``, ma to Wam pokazać że
  Wasz komputer może was okłamać przy wyświetlaniu strony (tak może też Wam
  pokazać żółtą kłódkę, ale to wymaga jeszcze paru operacji).

Z punktu widzenia ``nginxa`` starczy dodać kolejną sekcję ``server``, która dodatkowo:

1. Nie posiada określenego ``default_server``
2. Ma inną dyrektywę ``server_name``
3. Ma na przykład inną dyrektywę ``root``

My dodatkowo włączymy opcję ``autoindex`` która spowoduje automatyczne generowanie
listingu plików w kagalogu.

Konfiguracja jest w tym wypadku nietrywialna, ale składa
się z następujących etapów:


1. Dodajemy do pliku ``/etc/hosts`` nowe mapowanie, np.:

    127.0.1.2 onet.pl

2. Stworzyć w katalogu ``sites-avilable`` plik o nazwie ``nowy-onet``, o
   takiej zawartości jak plik ``default``.
3. W pliku ``nowy-onet`` należy:

   1. Zmienić w dyrektywie ``server_name`` wartość domeny na ``onet.pl``
      (teraz nginx będzie używał
      tej strony jeśli odbierze połączenie na adres ``onet.pl``.
   2. Usunąć z dyrektyw ``listen 80`` dyrektywę ``default_server``.

      .. note::

        Jeśli nginx odbierze połączenie na domenę niepodpiętą pod żaden
        blok server, do obsługi wykorzysta ten blok server, który ma podane
        ``default_server``. Może być dokładnie jeden taki plik
   3. Zmienić ``root`` na ``/usr/share/doc``.
   4. Blok location zmienić na::

        location / {
          autoindex on; # Włącza automatyczne listowanie katalogów
        }

      Spowoduje on, że po wejściu na adres ``onet.pl`` dostaniecie listing zawartości
      katalogu ``/usr/share/doc``.
4. Stworzyć dowiązanie symboliczne między ``/etc/nginx/sites-available/nowy-onet``
   ``/etc/nginx/sites-enabled/nowy-onet``.

   .. note::

    Do stworzenia dowiązania symbolicznego służy komenda ``ln -s skąd dokąd``.

    Zarówno nginx, jak i apache2 przechowują "możliwe do użycia serwisy" w katalogu
    ``sites-available``, a w sites enabled są dowiązania to tych stron, które są
    "aktualnie włączone".
6. Po zmianach zrestartować server ``service nginx restart``
7. Wejśc na stronę ``onet.pl``.

.. note::

  Czasem bardzo wygodne jest skopiowanie sobie dokumentacji języków programowania
  na swój komputer, i serwowanie ich z lokalnego serwera.

  Można to łatwo zrobić dodając do konfiguracji serwera::

    location /doc/ {
      alias /usr/share/doc/;
      autoindex on;
      allow 127.0.0.1;
      allow ::1;
      deny all;
    }

Zadanie F
*********

Za pomocą polecenia telnet wykonać zapytanie GET, które pobierze
zawartość ze strony ``localhost`` oraz modyfikowanej przez nas ``onet.pl``.

Protokół SMTP
-------------

Protokół SMTP służy do wysłania wiadomości e-mail, przykładowa konwersacja SMTP
wygląda tak::

  EHLO gmail.com
  250-mx.google.com at your service, [83.24.214.204]
  250-SIZE 35882577
  250-8BITMIME
  250-AUTH LOGIN PLAIN XOAUTH XOAUTH2 PLAIN-CLIENTTOKEN
  250-ENHANCEDSTATUSCODES
  250 CHUNKING
  AUTH LOGIN
  334 VXNlcm5hbWU6
  c2llY2lrb21wdXRlcm93ZXRlc3QyMDE0
  334 UGFzc3dvcmQ6
  redacted
  235 2.7.0 Accepted
  MAIL FROM: <siecikomputerowetest2014@gmail.com>
  250 2.1.0 OK g9sm392098wja.39 - gsmtp
  RCPT TO: <jbzdak@gmail.com>
  250 2.1.5 OK g9sm392098wja.39 - gsmtp
  DATA
  354  Go ahead g9sm392098wja.39 - gsmtp
  Subject:Test

  Tekst Test

  .

Wysyłanie wiadomości e-mail dokonuje się za pomocą protokołu tekstowego.

Konwersacja z serwerem zaczyna się od prośby o funkcojonalności serwera::

  EHLO gmail.com

.. note::

  To nie jest literówka, ale skrót od ``Extended HeLlO``.

Serwer odpowiada listą funkcjonalności, w tym że można logować się na kilka sposobów:

  250-AUTH LOGIN PLAIN XOAUTH XOAUTH2 PLAIN-CLIENTTOKEN

Klient wybiera najprostszą metodę logowania::

  AUTH LOGIN

.. note::

  Mamy tu typowy przykład **negocjacji w protokole**, serwer podaje jakie rodzaje
  autoryzacji są wspierane, a klient wybiera tego zestawu, ten rodzaj który mu
  odpowiada.

Serwer odpowiada::

  334 VXNlcm5hbWU6

Status 334 jest informacyjny i jest prośbą o nazwę użytkownika::

  c2llY2lrb21wdXRlcm93ZXRlc3QyMDE0

Następnie serwer prosi o hasło a klient je podaje::

  334 UGFzc3dvcmQ6
  redacted

.. note::

  Wiadomości które są częścią autoryzacji wyglądają na "zaszyfrowane" jednak
  wcale takie nie są. Są one zakodowane w kodowaniu base64::

    echo -n VXNlcm5hbWU6 | base64 -d
    Username:

  A login to::

    echo -n c2llY2lrb21wdXRlcm93ZXRlc3QyMDE0 | base64 -d
    siecikomputerowetest2014

Po zalogowaniu możemy wysyłać wiadomość, do wysłania wiadomości potrzebne są
takie komendy::

  MAIL FROM: <siecikomputerowetest2014@gmail.com>
  RCPT TO: <jbzdak@gmail.com>
  DATA
  Subject:Test

  Tekst Test

  .

Gdzie linia z ``.`` kończy wiadomość.

Bezpieczestwo SMTP
******************

SMTP ma zasadniczą wadę: *w zasadzie nie pozwala na potwierdzenie tożsamości nadawcy*.
Ciągle niektóre serwery pozwalają na przeprowadzenie takiej konwersacji::

  MAIL FROM: <prezydent@prezydent.pl>
  RCPT TO: <jbzdak@gmail.com>
  DATA
  Subject:Order

  Za wielkie zasługi dydaktyczne otrzymujesz order.

  .

Taka wiadomość zostanie często potraktowana jako spam, ale ma niezłą szansę
na dojście do obiorcy.

.. note::

  Najprawdopodobniej typowi dostawczy poczty (np. ``gmail``) nie pozwalają na
  wysłanie poczty z innego adresu niż ten zalogowany.

Przykład przesłanej wiadomości::

  Delivered-To: jbzdak@gmail.com
  Received: by 10.112.204.162 with SMTP id kz2csp980668lbc;
          Sat, 12 Dec 2015 11:36:32 -0800 (PST)
  X-Received: by 10.112.36.130 with SMTP id q2mr10069551lbj.116.1449948992830;
          Sat, 12 Dec 2015 11:36:32 -0800 (PST)
  Return-Path: <prezydent@prezydent.pl>
  Received: from mars.if.pw.edu.pl (mars.if.pw.edu.pl. [194.29.174.13])
          by mx.google.com with ESMTPS id r141si13413887lfg.219.2015.12.12.11.36.32
          for <jbzdak@gmail.com>
          (version=TLS1 cipher=AES128-SHA bits=128/128);
          Sat, 12 Dec 2015 11:36:32 -0800 (PST)
  Received-SPF: fail (google.com: domain of prezydent@prezydent.pl does not designate 194.29.174.13 as permitted sender) client-ip=194.29.174.13;
  Authentication-Results: mx.google.com;
         spf=fail (google.com: domain of prezydent@prezydent.pl does not designate 194.29.174.13 as permitted sender) smtp.mailfrom=prezydent@prezydent.pl
  Received: from mars.if.pw.edu.pl (dkd82.neoplus.adsl.tpnet.pl [83.24.7.82])
    (authenticated bits=0)
    by mars.if.pw.edu.pl (8.13.1/8.13.1) with ESMTP id tBCJZH7d004469
    (version=TLSv1/SSLv3 cipher=DHE-RSA-AES256-SHA bits=256 verify=NO)
    for jbzdak@gmail.com; Sat, 12 Dec 2015 20:36:04 +0100
  Date: Sat, 12 Dec 2015 20:35:17 +0100
  From: prezydent@prezydent.pl
  Message-Id: <201512121936.tBCJZH7d004469@mars.if.pw.edu.pl>
  Subject: PowoĹanie
  (...)

  Idziesz do wojska!


Wiadomość zdaje się być wysłana przez: ``prezydent@prezydent.pl``, tak twierdzi
nagłowek ``From:``::

  From: prezydent@prezydent.pl

Możemy sprawdzić że coś jest z wiadomością nie tak patrząc przez jakie serwery
poczta przeszła, pierwszym jest::

    Received: from mars.if.pw.edu.pl (mars.if.pw.edu.pl. [194.29.174.13])
          by mx.google.com with ESMTPS id r141si13413887lfg.219.2015.12.12.11.36.32
          for <jbzdak@gmail.com>
          (version=TLS1 cipher=AES128-SHA bits=128/128);
          Sat, 12 Dec 2015 11:36:32 -0800 (PST)

Na zdrowy rozum okazuje się, że mejle z domeny ``prezydent.pl`` nie powinny
wychodzić z adresu ``mars.if.pw.edu.pl``.

.. note::

  Bezpieczeństwo poczty znacznie wzrosło od czasu wprowadzenia mechanizmu
  SPF, który pozwala na identyfikację wiadomości wysłanych ze złych serwerów::

    Received-SPF: fail (google.com: domain of prezydent@prezydent.pl does not designate 194.29.174.13 as permitted sender) client-ip=194.29.174.13;

Zadanie G
*********

Za pomocą programu telnet, lub openssl spróbować wysłać pocztę ze swojej
poczty (lub poczty pokazanej przez prowadzącego).


By wysłać pocztę musicie:

* Połączyć się z odpowiednim serwerem, oraz odpowiednim portem.
* Jeśli chcecie się połączyć bez szyfrowania starczy użyć programu telnet,
  jeśli serwer wymaga szyfrowania możecie użyć polecenia:
  ``openssl s_client -quiet -connect host:port -crlf``.
* Następnie wysłać polecenia SMTP.

Jeśli poczta używa szyfrowania (SSL/TLS) do uzyskania połączenia konieczne
jest użycie programu openssl, by połączyć się z serwerami ``gmaila`` należy
wykonać::

  openssl s_client -quiet -connect host:port -crlf

W przypadku braku szfrowania::

  telnet domena port

By "ukryć" za pomocą base64 hasło i login można użyć programu base64, np::

  echo -n username | base64

Po wykonaniu wszystkich operacji dobrze jest usunąć historię basha::

  rm ~/.bash_history

