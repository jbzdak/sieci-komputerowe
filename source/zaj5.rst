Zajęcia 5: Administracja systemem Linux
=======================================

Użytkownik administracyjny
--------------------------

W GNU/Linuksie istnieje bardzo mocno wymuszany podział na administratora komputera,
oraz zwykłego użytkownika (w nowszych windowsach, jak słyszałem, również tak
jest).

Do wykonywania czynności administracyjnych stworzono użytkownika o nazwie ``root``,
o specjalnym id równym ``0``.

Na nowszych systemach konto administracyjne jest domyślnie wyłączone, a jego
uprawnienia zdobywa się tymczasowo, z użyciem komendy ``sudo``.

Komenda sudo
------------

Komenda sudo pozwala aktualnemu użytkownikowi (o ile spełnia dodatkowo
pewne wymagania) wykonywać zadania administracyjne.

Przykłady::

  sudo adduser jbzdak # Wykona polecenie adduser jbzdak z uprawnieniami administratora
  sudo -i # Zalogouje się do konsoli administratora
  sudo -u user -i # Uruchomi konosolę z zalogowanym użytkownikiem user

Instalowania oprogramowania
---------------------------

Wszystkie sytemy operacyjne Linux przychodzą z (mniej lub bardziej bogatym)
zestawem oprogramowania, oraz narzędziami do automatycznej instalacji
wspomnianego oprogramowania.

Ma to wiele zalet:

* Jest znacznie bezpieczniejsze niż pobieranie pliku ``setup.exe`` **z Interntu**.
  Pakiety (przynajmniej w tych dystrybucjach które znam) posiadają system
  *kryptograficznej weryfikacji*.
* Znacznie ułatwia zarządzanie wieloma serwerami.
* Znacznie ułatwia zarządzanie wersjami bibliotek


System paczek debiana oparty jest o programy ``dpkg`` oraz ``apt``.

.. note::

  Niektóre dystrybucje (np. Ubuntu) posiadają całkiem `użyteczne graficzne
  nakładki <https://en.wikipedia.org/w/index.php?title=Ubuntu_Software_Center&oldid=689180699>`__
  na system pakietów, nakładki te przypominają "Sklepy" znane z
  polularnych marek smartfonów.


  Debian posiada też inne narzędzia do pracy z Pakietami, np. bardzo dobry
  ``aptitude`` (tekstowy lub graficzny) oraz graficzny ``synaptic``,
  jednak w "pierwszym kontakcie" mogą obe być mniej intuicyjne w użyciu



Repozytoria oprogramowania
**************************

Żródła z których pobieramy oprogramowanie są opisane w pliku:
``/etc/apt/sources.list`` (oraz w plikach w: ``/etc/apt/sources.list.d``).

Można dopisywać do tych plików repozytoria będące poza kontrolą dostawcy
Waszej dystrybucji np. by zainstalować najnowszą wersję przeglądarki.

Aktualnie Wasze maszyny wirtualne mogą być skonfigurowane do pobierania pakietów
z płyty CD.

Konfigiracja managera pakietów powinna wyglądać tak::

  deb http://ftp.pl.debian.org/debian/ jessie main non-free contrib
  deb http://security.debian.org/ jessie/updates main contrib non-free
  deb http://ftp.pl.debian.org/debian/ jessie-updates main non-free contrib

Instalujemy pakiety z aktuanej stabilnej wersji Debiana (``jessie``).

Repozytorium debiana podzielone jest na sekcje:

* ``main`` główna sekcja oprogramowania wspierana przez zespół Debiana
* ``contrib`` oprogramowanie na wolnej licencji nie wspierane przez zespół debiana
* ``non-free`` oprogramowanie nie wydane na wolnej licencji

.. note::

  Na komputer "domowy" możecie śmiało użyć tej konfigurajci, na serwer warto się 
  zastanowić czego potrzebujecie.

Zadania A
*********

Za pomocą sudo oraz Waszego ulubionego edytora tekstu ustawcie zawartość 
pliku ``/etc/apt/sources.list`` na::

  deb http://ftp.pl.debian.org/debian/ jessie main non-free contrib
  deb http://security.debian.org/ jessie/updates main contrib non-free
  deb http://ftp.pl.debian.org/debian/ jessie-updates main non-free contrib

Przeszukiwanie pakietów
-----------------------

Po zmianie konfiguracji repozytorióœ nalezy pobrać informacje o pakietów
z repozytorium::

  sudo apt-get update

Komenda ``sudo apt-get update`` pobiera również informacje o nowych pakietach
które wydali twórcy Debiana i dobrze ją wykonać przed każdą czynnością 
dotyczącą pakietów.

Do przeszukiwania repozytorium pakietów służy program ``apt-cache``.

By wyszukać pakietów które mają w nazwie lub opisie ciąg znaków foo należy
napisać polecenie::

  apt-cache search foo

By wyszukać tylko w nazwie pakietu należy napisać::

  apt-cache search -n foo

Zadanie B
*********

Odnaleźć nazwę pakietu który dostarcza serwer ``ssh``, w tym celu proszę 
przeszukać paczki które w nazwie mają ssh a następnie za pomocą ``grep``
znaleźć paczki które w nazwie mają ``server``.

Za pomocą ``apt-get install`` zainstalować server ssh.

Serwer ``ssh``
--------------

SSH to standard protokołów komunikacyjnych, które umożliwiają:

* Bezpieczną zdalną pracę na komputerze podłączonym do Internetu
* Bezpieczne logowanie za pomocą pary kryptograficznych kluczy
* Tworzenie bezpiecznych tuneli pozwalających na np. omijanie niektórych
  firewalli.

Konfiguracja serwera ``ssh``
----------------------------

Konfiguracja serwera mieści się w katalogu ``/etc/ssh/sshd_config``
(proszę nie pomylić z ``ssh_config`` --- bez ``d`` w środku).

By zapoznać się z tym co można skonfigurować w tym pliku, proszę przejrzeć 
stronę manuala: ``man sshd_config``.

Zadanie C
*********

Wyłączyć logowanie ssh dla użytkowników ``foo`` oraz ``root``.

Po wykonaniu zmian należy zrestartować serwer wykonując polecenie::

  sudo service ssh restart

Logowanie za pomocą klucza
--------------------------

Logowanie za pomocą klucza publicznego jest "lepszą" alternatywą dla logowania
za pomocą haseł.

Logowanie się na zdalnej maszynie
*********************************

Logowanie się na zdalnej maszyny::

  ssh username@ip

Przy pierwszym logowaniu pojawi się ostrzeżenie::

  The server's host key is not cached in the registry. You
  have no guarantee that the server is the computer you
  think it is.
  The server's rsa2 key fingerprint is:
  ssh-rsa 2048 xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:

Mówi ono że:

* Nie łączyliście się z tym serwerem do tej pory.
* System SSH nie ma pewności, że połączyliście się z właściwym serwerem
  (SSH potrafi zweryfikować że połączyliście się z serwerem posiadającym dany
  klucz publiczny).

Działanie logowania za pomocą klucza
************************************

Klient generuje parę kluczy, zwanych publicznym i prywatnym, klucze te
(nie będę wchodził w kryptograficzne szczegóły) pozwalają na wykonanie
kryptograficznego podpisu jakiejś wiadomości. Klucz prywatny pozwala na podpisanie
wiadomości, a publiczny na weryfikację poprawności podpisu. Mając klucz publiczny
nie da się ani podpisać wiadomości, ani uzyskać klucza prywatnego.

.. note::

  Procedura logowania za pomocą klucza wygląda tak:

  * Klient i serwer negocjują połączenie ``1`` do ``1``, którego nikt nie może
    podsłuchać.
  * Serwer udowadnia klientowi, że jest tym serwerem do którego klient chciał się
    połączyć (zaraz o tym jak to robi)
  * Klient wysyłą prośbę: ``chcę zalogować się jako użytkownik taki a taki``.
  * Serwer odpowiada: w takim razie podpisz mi ten losowy ciąg znaków.
  * Klient swoim kluczem prywatnym podpisuje ów losowy ciąg znaków.
  * Serwer znajduje odpowiedni klucz publiczny i sprawdza podpis.

Klucze mają następujące przewagi nad hasłami:

* Bezpieczne hasła są bardzo trudne do zapamiętania. Klucze są zapamiętywane
  przez komputer, więc mogą zawierać więcej "losowości".
* Hasło jest wysyłane w tekście jawnym na serwer, w autoryzacji kluczem publicznym
  w komunikacji nie pojawiają się dane które pozwalają na autoryzację.
* Hasło trzeba podawać przy każdym kolejnym logowaniu, istnieją natomiast
  dość standardowe metody bezpiecznego przechowywania kluczy ssh w pamięci ram
  komputera (hasło do klucza podaje się tylko raz przy logowaniu na komputer;
  my na zajęciach będziemy używali kluczy bez hasła)

Instalacja swojego klucza na serwerze
*************************************

By wygenerować klucz należy wykonać polecenie ``ssh-keygen``, można podać 
hasło do klucza, ale na dziś sobie to darujemy.

Polecenie ``ssh-keygen`` stworzyło dwa pliki:

* ``.ssh/id_rsa.pub`` --- zawiera klucz publiczny
* ``.ssh/id_rsa`` --- zawiera klucz prywatny

Plik ``$HOME/.ssh/authorized_keys`` zawiera listę kluczy publicznych, po jednym
w każdej linii. Klucz prywatny powiązany z dowolnym z tych kluczy publicznych
może posłużyć do logowania się do serwera.

Zadanie D
*********

Zalogować się na maszynę virtualną za pomocą klucza ssh.

Instalacja firewalla
--------------------

Firewall to oprogramowanie pozwalające na kontrolę pakietów IP przetwarzanych
przez maszynę.

W systemach GNU/Liunux firewall jest częścią jądra systemu i można go
kontrolować za pomocą polecenia ``iptables``, polecenie to jednak nie jest
bardzo przyjazne, więc poznamy bardzo prostą nakładkę na ``iptables``:
mianowicie program ``ufw``.

Zadanie E
*********

Proszę zainstalować firewall ``ufw``, a następnie korzystając z informacji
z ``man ufw``:

1. Wyłączyć możliwość wykonywania połączeń przychodzących
2. Zweryfikować brak możliwości podłączania się z serwerem ssh
3. Umożliwić łączenie się na port ``22`` (port SSH)
4. Zweryfikować że ssh działa
5. Wyłączyć firewall ufw.


Instalacja serwera i klienta NFS
--------------------------------

NFS to sieciowy system plików, w zasadzie jeden z wielu siecowych systemów
plików, przykłady innych to:

* Samba (protokółw spółdzielenia plików i innych rzeczy w Windowsach)
* Lustre (klastrowy system plików)

Uczymy się NFS bo jest najprostszy, ma on sporo wad i równie wiele zalet.


Dodanie nowego interfejsu sieciowego
************************************

* Wyłączamy maszyny wirtualne
* Settings -> Network Adapters -> Wybieramy trzeci z kolei -> Attached to:
  Bridged Adapter, eth0.
* Startujemy maszyny.
* ``sudo dhclient eth2``

Instalacja serwera nfs
**********************

By zainstalować narzędzia nfs należy zainstalować dwie paczki: ``nfs-kernel-server``
oraz ``nfs-common``, pierwsza służy do udostępniania katalgoów, druga do
montowania katalogów już udostępnionych.

Zadanie F
*********

Proszę:

1. Zainstalować NFS
2. Konfiguracja NFS jest w katalogu ``/etc/exports``, korzystając z ``man exports``
   udostępnić katalog ``/home/<<waszlogin>>/foo`` dla całego laboratorium
   (tj. dla sieci ``192.168.1.0/24``), do odczytu i zapisu
3. Zamontować sobie katalog który udostępnił jakaś koleżanka/kolega i następnie
   zapisać coś w tym katalogu.

   Do montowania plików i katalogów służy polecenie ``mount <skąd> <dokąd>``,
   ``<dokąd>`` musi być **istniejącym** katalogiem lokalnym. ``<skąd>``
   w przypadku nfs ma postać ``server:/ścieżka``.










